#include "nameLabel.h"

NameLabel::NameLabel(
	Board* board, Player* player, TTF_Font* font,
	SDL_Rect box, SDL_Surface* screen
):Label("?", font, box, screen) {
	this->player = player;
	subject = board;
	subject->attach(this);
}

NameLabel::~NameLabel() {
	subject->detach(this);
}

Player* NameLabel::getPlayer() {
	return player;
}

void NameLabel::setPlayer(Player* player) {
	this->player = player;
	setCaption(this->player->getName());
}

void NameLabel::update(Observable* theChangedSubject) {
	if(theChangedSubject != subject) {
		return;
	}
	setCaption(player->getName());
	if(!player->isSeated()) {
		setCaption("?");
	}
}

