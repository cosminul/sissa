#ifndef GUI_UTILS_H
#define GUI_UTILS_H

#include "SDL/SDL.h"
#include <string>

SDL_Surface *loadImage(std::string imgPath, std::string filename);

void applySurface(
	int x, int y, SDL_Surface* source, SDL_Surface* destination,
	SDL_Rect* clip = NULL
);

SDL_Rect mkRect(int x, int y);

SDL_Rect mkRect(int x, int y, int w, int h);

#endif

