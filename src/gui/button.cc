#include "button.h"

void Button::init() {
	this->obj = NULL;
	this->arg = NULL;
	this->function0 = NULL;
	this->function1 = NULL;
}

Button::Button():Component() {
	init();
}

Button::Button(SDL_Rect box, SDL_Surface* screen):Component(box, screen) {
	init();
}
	
Button::Button(
	SDL_Rect box, SDL_Surface* screen,
	void* obj, void (*function0)(void* obj)
):Component(box, screen) {
	init();
	this->obj = obj;
	this->function0 = function0;
}

Button::Button(
	SDL_Rect box, SDL_Surface* screen,
	void* obj, void* arg, void (*function1)(void* obj, void* arg)
):Component(box, screen) {
	init();
	this->obj = obj;
	this->arg = arg;
	this->function1 = function1;
}

Button::~Button() {
	// TODO bye bye, button
}

bool Button::isInside(int x, int y) {
	bool over = true;
	over = over && (x > box.x + BORDER_WIDTH);
	over = over && (x < box.x + box.w - BORDER_WIDTH);
	over = over && (y > box.y + BORDER_WIDTH);
	over = over && (y < box.y + box.h - BORDER_WIDTH);
	return over;
}

void Button::mouseMove(SDL_MouseMotionEvent event) {
	// TODO implement this method
}

void Button::mouseDown(SDL_MouseButtonEvent event) {
	// If the left mouse button was pressed
	if(event.button == SDL_BUTTON_LEFT) {
		// If the mouse is over the square
		if(isInside(event.x, event.y)) {
			// Set the square sprite
			clip = &clips[CLIP_MOUSEDOWN];
		}
	}
}

void Button::mouseUp(SDL_MouseButtonEvent event) {
	// If the mouse is over the button
	if(isInside(event.x, event.y)) {
		// Set the square sprite
		if(event.button == SDL_BUTTON_LEFT) {
			// Set the square sprite
			clip = &clips[CLIP_MOUSEUP];
		}
		if(function1 != NULL) {
			function1(obj, arg); // make callback
		} else if(function0 != NULL) {
			function0(obj);
		}
	}
}

void Button::handleEvents(SDL_Event event) {
	// If a mouse button was moved
	if(event.type == SDL_MOUSEMOTION) {
		mouseMove(event.motion);
	}

	// If a mouse button was pressed
	if(event.type == SDL_MOUSEBUTTONDOWN) {
		mouseDown(event.button);
	}

	// If a mouse button was released
	if(event.type == SDL_MOUSEBUTTONUP) {
		mouseUp(event.button);
	}
}

