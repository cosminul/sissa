#ifndef MENU_H
#define MENU_H

#include <stack>
#include <vector>
#include "observer.h"
#include "board.h"
#include "player.h"
#include "component.h"
#include "menuItem.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"

class MenuItem;

class Menu : public Component, public Observer {
private:
	std::stack<MenuItem*> items;

	/** The available roots */
	std::vector<MenuItem*> roots;

	/** The current root */
	MenuItem* root;

	TTF_Font* fonts[3];

	TTF_Font* font;

	char result[512];

	Board* subject; // the observable

	Player* owner;

public:
	friend class MenuItem;

	void setResult(const char* result);

public:
	static const int FONT_NORMAL = 0;
	static const int FONT_HIGHLIGHT = 1;
	static const int FONT_LABEL = 2;

	Menu(
		Board* board, Player* owner, TTF_Font* fonts[],
		SDL_Rect box, SDL_Surface* screen
	);

	~Menu();

	TTF_Font* getFont(int index);

	bool isEmpty();

	void empty();

	void reset();

	void push(MenuItem* item);

	MenuItem* pop();

	MenuItem* top();

	char* getResult();

	/**
	 * Callback to non-static member `pop()',
	 * implemented as a static wrapper.
	 * @param object the this-pointer of a class object
	 */
	static void cmdGoBack(void* object);

	void addRoot(MenuItem* item);

	void setRootMenuItem(MenuItem* item);

	void setRootMenuItem(std::string rootName);

	void translate();

	/** Handles events */
	void handleEvents(SDL_Event event);

	void update(Observable* theChangedSubject);

	void show();
};

#endif

