#ifndef COMPONENT_H
#define COMPONENT_H

#include "SDL/SDL.h"

/**
 * Component - an abstract widget
 * @author Cosmin
 * @date 2007 03 18
 */
class Component {
protected:
	/** The position and dimensions of the button */
	SDL_Rect box;

	/** The part of the sprite sheet that will be shown */
	SDL_Rect* clip;

	/** The clips (array) */
	SDL_Rect* clips;

	/** The sprite sheet */
	SDL_Surface* spriteSheet;

	/** The screen */
	SDL_Surface* screen;

	bool visible;

	/**
	 * This constructor is protected because it does not define the
	 * box and the screen of the component. It will only be used by
	 * descendants.
	 */
	Component();

public:
	Component(SDL_Rect box, SDL_Surface* screen);

	virtual ~Component();

	void setSpriteSheet(SDL_Surface* sheet);

	SDL_Surface* getScreen();

	virtual void setScreen(SDL_Surface* screen);

	virtual void setClips(SDL_Rect* clips);

	virtual void setClips(SDL_Rect* clips, SDL_Rect* defaultClip);

	/**
	 * Return the position and the dimensions of the component
	 */
	virtual SDL_Rect getBox();

	virtual bool isVisible();

	virtual void setVisible(bool visible);

	/** Shows the component on the screen */
	virtual void show();
};

#endif

