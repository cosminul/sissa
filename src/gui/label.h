#ifndef LABEL_H
#define LABEL_H

#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "component.h"

/**
 * Label - a widget that displays a text on an SDL_Surface.
 * @author Cosmin
 * @date 2007 03 17
 */
class Label : public Component {
protected:
	/** The text that's going to be displayed */
	std::string caption;

	/** The font that's going to be used */
	TTF_Font* font;

	/** The color of the font */
	SDL_Color textColor;

	/** The background color */
	SDL_Color background;

	int quality;

	virtual void render();

public:
	static const int SOLID = 0;
	static const int SHADED = 1;
	static const int BLENDED = 2;

	Label(std::string caption);

	Label(std::string capt, TTF_Font* font, SDL_Rect box, SDL_Surface* scr);

	~Label();

	std::string getCaption();

	virtual void setCaption(std::string caption);

	int getX();

	void setX(int x);

	int getY();

	void setY(int y);

	void setPosition(int x, int y);

	void setColor(int red, int green, int blue);

	void setBackground(int red, int green, int blue);

	TTF_Font* getFont();

	void setFont(TTF_Font* font);

	void setQuality(int quality);
};

#endif
