#ifndef NAME_LABEL_H
#define NAME_LABEL_H

#include "observer.h"
#include "player.h"
#include "label.h"
#include "SDL/SDL_ttf.h"

class NameLabel : public Label, public Observer {
private:
	Player* player;

	Board* subject;

public:
	NameLabel(
		Board* board, Player* player, TTF_Font* font,
		SDL_Rect box, SDL_Surface* screen
	);

	~NameLabel();

	Player* getPlayer();

	void setPlayer(Player* player);

	void update(Observable* theChangedSubject);
};

#endif

