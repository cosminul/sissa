#ifndef MENU_ITEM_H
#define MENU_ITEM_H

#include <string>
#include <vector>
#include "SDL/SDL.h"
#include "button.h"
#include "label.h"
#include "menu.h"

class Menu;

class MenuItem : public Button {

protected:
	/** The text that can be translated to obtain an i15d caption */
	std::string text;

	Label* content;

	std::vector<MenuItem*> children;

	Menu* menu;

	SDL_Rect menuBox;

	bool enabled;

private:
	/**
	 * Non-public member function that helps to avoid reduplicating
	 * identical pieces of contructors' code 
	 */
	void init();

public:
	/**
	 * @param the caption
	 * @param the font
	 * @param the box
	 * @param the screen
	 */
	MenuItem(
		std::string caption, TTF_Font* font, SDL_Rect box,
		SDL_Surface* screen, void* obj, void* arg,
		void (*function)(void* obj, void* arg)
	);

	MenuItem(
		std::string caption, TTF_Font* font, SDL_Rect box,
		SDL_Surface* screen, void* obj, void (*function)(void* obj)
	);

	MenuItem(
		std::string caption, void* obj, void* arg,
		void (*function)(void* obj, void* arg)
	);

	MenuItem(std::string caption, void* obj, void (*function)(void* obj));

	MenuItem(std::string);

	virtual ~MenuItem();

	std::string getCaption();

	void setCaption(std::string);

	virtual void setScreen(SDL_Surface*);

	virtual void setFont(TTF_Font*);

	virtual void setMenu(Menu* menu);

	void setEnabled(bool enabled);

	/** Tells whether the some coordinates are inside the button */
	bool isInside(int x, int y);

	virtual void handleEvents(SDL_Event event);

	void handleChildrenEvents(SDL_Event event);

	void mouseMove(SDL_MouseMotionEvent event);

	void mouseDown(SDL_MouseButtonEvent event);

	void mouseUp(SDL_MouseButtonEvent event);

	virtual void show();

	void showChildren();

	void add(MenuItem*);

	void remove(MenuItem*);

	void distributeChildren();

	virtual void translate();

	void execute();
};

#endif

