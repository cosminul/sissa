#ifndef G_SQUARE_H
#define G_SQUARE_H

#include <string>
#include "SDL/SDL.h"
#include "button.h"
#include "square.h"
#include "king.h"
#include "queen.h"
#include "rook.h"
#include "bishop.h"
#include "pawn.h"
#include "knight.h"

/**
 * gSquare - graphical square
 * @author Cosmin
 * @date 2007 01 12
 */

class GBoard;

class GSquare : public Button {
private:
	SDL_Rect* pClip;

	SDL_Rect* pClips;
	
	SDL_Surface* pieceSheet;

	/** A pointer to the real square */
	Square* square;

	/** A pointer to the graphical board */
	GBoard* gBoard;

	bool selected;

	/**
	 * Left click handler. If no square is selected, selects this.
	 * If a square is selected, tries to move here.
	 */
	void selectOrMove();

protected:
	void mouseDown(SDL_MouseButtonEvent event);

	void mouseUp(SDL_MouseButtonEvent event);

public:
	// The square states in the sprite sheet
	static const int CLIP_VALID = 2;
	static const int CLIP_THREAT = 3;

	// The width of the border, in pixels
	static const int BORDER_WIDTH = 2;

	/**
	 * Initialize the variables
	 */
	GSquare(GBoard* gBoard, Square* square, SDL_Rect box, SDL_Surface* screen);

	~GSquare();

	/**
	 * Returns the position of the square. Example: a1, g5
	 */
	std::string getPosition();

	void setBox(SDL_Rect box);

	Square* getSquare();

	/**
	 * Sets the piece on this square. It is actually just the index
	 * of an clip in the sprite sheet.
	 * @param piece CLIP_*
	 */
	void setPiece(int piece);

	void setPieceSheet(SDL_Surface* sheet);

	void setPieceClips(SDL_Rect* clips);

	bool isSelected();

	void setSelected(bool selected);

	void showValidMoves();

	void markAsValid();

	/** Shows the square on the screen */
	void show();
};

#endif

