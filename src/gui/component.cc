#include "component.h"
#include "guiUtils.h"

Component::Component() {
	box = mkRect(0, 0);
	spriteSheet = NULL;
	screen = NULL;
	clips = NULL;
	clip = NULL;
	visible = true;
}

Component::Component(SDL_Rect box, SDL_Surface* screen) {
	this->box = box;

	spriteSheet = NULL;
	this->screen = screen;
	clips = NULL;

	// Set the default sprite
	clip = NULL;

	visible = true;
}

Component::~Component() {
}

void Component::setSpriteSheet(SDL_Surface* sheet) {
	spriteSheet = sheet;
}

SDL_Surface* Component::getScreen() {
	return screen;
}

void Component::setScreen(SDL_Surface* screen) {
	this->screen = screen;
}

void Component::setClips(SDL_Rect* clips) {
	this->clips = clips;
}

void Component::setClips(SDL_Rect* clips, SDL_Rect* defaultClip) {
	setClips(clips);
	clip = defaultClip;
}

SDL_Rect Component::getBox() {
	return box;
}

bool Component::isVisible() {
	return visible;
}

void Component::setVisible(bool visible) {
	this->visible = visible;
}

void Component::show() {
	if(visible) {
		applySurface(box.x, box.y, spriteSheet, screen, clip);
	}
}

