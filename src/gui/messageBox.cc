#include "messageBox.h"
#include "player.h"
#include "lang.h"

void MessageBox::init() {
	content->setBackground(229, 169, 112);
	content->setQuality(Label::BLENDED);
	visible = false;
	popTime = 0;
	delay = DEFAULT_DELAY;
	render();
}

MessageBox::MessageBox(TTF_Font* font, SDL_Rect box, SDL_Surface* screen)
:Button(box, screen) {
	content = new Label("", font, box, screen);
	content->setFont(font);
	init();
}

MessageBox::MessageBox(
	Board* subject, Player* owner,
	TTF_Font* font, SDL_Rect box, SDL_Surface* screen
):Button(box, screen) {
	this->subject = subject;
	subject->attach(this);
	this->owner = owner;
	content = new Label("", font, box, screen);
	content->setFont(font);
	init();
}

MessageBox::~MessageBox() {
	subject->detach(this);
}

std::string MessageBox::getCaption() {
	return content->getCaption();
}

void MessageBox::setCaption(std::string caption) {
	content->setCaption(caption);
	render();
}

void MessageBox::setScreen(SDL_Surface* screen) {
	content->setScreen(screen);
}

void MessageBox::setFont(TTF_Font* font) {
	content->setFont(font);
	render();
}

Uint32 MessageBox::getDelay() {
	return delay;
}

void MessageBox::setDelay(Uint32 delay) {
	this->delay = delay;
}

void MessageBox::show() {
	// Most of the time, the widget is invisible
	if(visible == false) {
		return;
	}
	Component::show();
	content->show();
	// Is this the first call of the function since becoming visible?
	if(popTime == 0) {
		popTime = SDL_GetTicks();
	} else if(SDL_GetTicks() - popTime >= delay) {
		visible = false;
		popTime = 0;
	}
}

void MessageBox::render() {
	const char* text = content->getCaption().c_str();
	// Find out the length of the text, in pixels
	int w = 0, h = 0;
	TTF_SizeUTF8(content->getFont(), text, &w, &h);
	// Align the text
	content->setX(box.x + (box.w - w) / 2);
	content->setY(box.y + (box.h - h) / 2);
}

void MessageBox::pop(std::string message) {
	setCaption(message);
	visible = true;
}

void MessageBox::update(Observable* theChangedSubject) {
	if(theChangedSubject != subject) {
		return;
	}
	int msg = subject->getMessage();

	std::string me;
	std::string other;
	if(subject->getCurrent() == subject->getWhite()) {
		me = i18n("White");
		other = i18n("Black");
	} else {
		me = i18n("Black");
		other = i18n("White");
	}

	switch(msg) {
	case Board::MSG_CHECK:
		if(subject->getCurrent() == owner) {
			pop(i18n("Check!"));
		}
		break;
	case Board::MSG_CHECKMATE:
		if(subject->getCurrent() == owner) {
			pop(i18n("Checkmate!"));
		}
		break;
	case Board::MSG_LEAVE:
		if(subject->getCurrent() != owner) {
			me.append(i18n(" has quit."));
			pop(me);
		}
		break;
	case Board::MSG_RESIGN:
		if(subject->getCurrent() != owner) {
			me.append(i18n(" has resigned."));
			pop(me);
		}
		break;
	case Board::MSG_PROMOTED:
		if(subject->getCurrent() == owner) {
			other.append(i18n(" promoted a pawn."));
			pop(other);
		}
		break;
	}
}

