#include "indicator.h"
#include "sissa.h"

Indicator::Indicator(
	Board* board, SDL_Rect box, SDL_Rect altBox, SDL_Surface* screen
):Component(box, screen) {
	savedPos1 = pos1 = box;
	savedPos2 = pos2 = altBox;
	subject = board;
	subject->attach(this);
}

Indicator::~Indicator() {
	subject->detach(this);
}

void Indicator::swapPositions() {
	SDL_Rect aux = pos1;
	pos1 = pos2;
	pos2 = aux;
	// Force refresh
	update(subject);
}

void Indicator::resetPositions() {
	pos1 = savedPos1;
	pos2 = savedPos2;
	// Force refresh
	update(subject);
}

void Indicator::update(Observable* theChangedSubject) {
	if(theChangedSubject != subject) {
		return;
	}
	if(subject->getCurrent() == NULL) {
		return;
	}
	if(subject->getCurrent()->getColor() == Player::WHITE) {
		box = pos1;
	} else {
		box = pos2;
	}
}

