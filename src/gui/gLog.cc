#include "gLog.h"
#include "textView.h"

GLog::GLog(Board* board, TTF_Font* font, SDL_Rect box, SDL_Surface* screen)
:Component(box, screen) {
	subject = board;
	TextView* textView = new TextView("", font, box, screen);
	textView->setBackground(229, 169, 112);
	content = textView;
	subject->attach(this);
}

GLog::~GLog() {
	subject->detach(this);
}

void GLog::update(Observable* theChangedSubject) {
	if(theChangedSubject != subject) {
		return;
	}
	content->setCaption(subject->getLogger()->toString());
}

void GLog::setScreen(SDL_Surface* screen) {
	content->setScreen(screen);
}

void GLog::show() {
	content->show();
}

