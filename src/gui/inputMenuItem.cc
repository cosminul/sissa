#include "inputMenuItem.h"
#include "guiUtils.h"
#include "lang.h"
#include <stdio.h>

void InputMenuItem::init() {
	blink = 0;
	labelText = "";
	label = new Label("", content->getFont(), mkRect(200, 200, 30, 30), content->getScreen());
	label->setBackground(229, 169, 112);
	label->setQuality(Label::BLENDED);
	input = content->getCaption();
}

InputMenuItem::InputMenuItem(
	std::string caption, TTF_Font* font, SDL_Rect box,
	SDL_Surface* screen, void* obj, void* arg,
	void (*function)(void* obj, void* arg)
):MenuItem(caption, font, box, screen, obj, arg, function) {
	init();
}

InputMenuItem::InputMenuItem(
	std::string caption, void* obj, void* arg,
	void (*function)(void* obj, void* arg)
):MenuItem(caption, obj, arg, function) {
	init();
}

InputMenuItem::InputMenuItem(std::string caption):MenuItem(caption) {
	init();
}

InputMenuItem::~InputMenuItem() {
}

void InputMenuItem::setMenu(Menu* menu) {
	MenuItem::setMenu(menu);
	// Set the default answer
	if(this->menu != NULL) {
		this->menu->setResult(content->getCaption().c_str());
	}
}

void InputMenuItem::setScreen(SDL_Surface* screen) {
	MenuItem::setScreen(screen);
	label->setScreen(screen);
}

void InputMenuItem::setFont(TTF_Font* font) {
	MenuItem::setFont(font);
	if(menu == NULL) {
		return;
	}
	label->setFont(menu->getFont(Menu::FONT_LABEL));
}

void InputMenuItem::setLabel(std::string caption) {
	labelText = caption;
	label->setCaption(i18n(caption));
}

void InputMenuItem::keyPress(SDL_KeyboardEvent event) {
	switch(event.keysym.sym) {
	case SDLK_BACKSPACE:
		// Backspace pressed, delete the last character
		input = input.substr(0, input.length() - 1);
		break;
	case SDLK_RETURN:
	case SDLK_KP_ENTER: // keypad enter
		menu->setResult(input.c_str());
		execute();
		break;
	default:
		char key;
		if((event.keysym.unicode & 0xFF80) == 0) {
			key = event.keysym.unicode & 0x7F;
		}
		char keyStr[2] = {key, '\0'};
		input.append(std::string(keyStr));
	}
	content->setCaption(input);
	// Center horizontally
	menu->top()->distributeChildren();
}

void InputMenuItem::handleEvents(SDL_Event event) {
	MenuItem::handleEvents(event);

	// If a key was pressed
	if(event.type == SDL_KEYDOWN) {
		keyPress(event.key);
	}
}

void InputMenuItem::show() {
	int blinkRate = 6;
	if(blink < blinkRate / 2) {
		char cursor[2] = {'_', '\0'};
		std::string tmp = content->getCaption();
		content->setCaption(tmp.append(std::string(cursor)));
		content->show();
		tmp = tmp.substr(0, tmp.length() - 1);
		content->setCaption(tmp);
	} else {
		content->show();
	}
	// Position the label
	int w = 0, h = 0;
	TTF_SizeUTF8(label->getFont(), label->getCaption().c_str(), &w, &h);
	label->setX(menuBox.x + (menuBox.w - w) / 2);
	label->setY(content->getY() - h);
	label->show();
	// Toggle blink value
	if(++blink >= blinkRate) {
		blink = 0;
	}
}

void InputMenuItem::translate() {
	MenuItem::translate();
	setLabel(labelText);
}

