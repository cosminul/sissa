#include "menuItem.h"
#include "guiUtils.h"
#include "lang.h"

#include <iostream>

void MenuItem::init() {
	content->setBackground(229, 169, 112);
	content->setQuality(Label::BLENDED);
	menu = NULL;
	children.reserve(3);
	enabled = true;
}

MenuItem::MenuItem(
	std::string caption, TTF_Font* font, SDL_Rect box,
	SDL_Surface* screen, void* object, void* arg,
	void (*function1)(void* object, void* arg)
):Button(box, screen, object, arg, function1) {
	text = caption;
	content = new Label(caption, font, box, screen);
	init();
}

MenuItem::MenuItem(
	std::string caption, TTF_Font* font, SDL_Rect box,
	SDL_Surface* screen, void* object, void (*function0)(void* object)
):Button(box, screen, object, function0) {
	text = caption;
	content = new Label(caption, font, box, screen);
	init();
}

MenuItem::MenuItem(
		std::string caption, void* object, void* arg,
		void (*function1)(void* object, void* arg))
:Button() {
	text = caption;
	content = new Label(caption);
	content->setPosition(200, 200);
	init();
	this->obj = object;
	this->arg = arg;
	this->function1 = function1;
}

MenuItem::MenuItem(
		std::string caption, void* object,
		void (*function0)(void* object))
:Button() {
	text = caption;
	content = new Label(caption);
	content->setPosition(200, 200);
	init();
	this->obj = object;
	this->function0 = function0;
}

MenuItem::MenuItem(std::string caption):Button() {
	text = caption;
	content = new Label(caption);
	content->setPosition(200, 200);
	init();
}

MenuItem::~MenuItem() {
}

std::string MenuItem::getCaption() {
	return content->getCaption();
}

void MenuItem::setCaption(std::string caption) {
	content->setCaption(caption);
}

void MenuItem::setScreen(SDL_Surface* screen) {
	content->setScreen(screen);
	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->setScreen(screen);
	}
}

void MenuItem::setFont(TTF_Font* font) {
	content->setFont(font);
	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->setFont(font);
	}
}

void MenuItem::setMenu(Menu* menu) {
	if(menu == NULL) {
		return;
	}
	this->menu = menu;

	content->setPosition(menu->getBox().x, menu->getBox().y);
	menuBox = menu->getBox();

	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->setMenu(menu);
	}
}

void MenuItem::setEnabled(bool enabled) {
	if(this->enabled == enabled) {
		return;
	}
	this->enabled = enabled;
	if(enabled) {
		content->setColor(0, 0, 0);
	} else {
		content->setColor(96, 96, 96);
	}
}

bool MenuItem::isInside(int x, int y) {
	SDL_Rect labelBox = content->getBox();
	bool over = true;
	over = over && (x > labelBox.x);
	over = over && (x < labelBox.x + labelBox.w);
	over = over && (y > labelBox.y);
	over = over && (y < labelBox.y + labelBox.h);
	return over;
}

void MenuItem::show() {
	content->show();
}

void MenuItem::showChildren() {
	distributeChildren();
	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->show();
	}
}

void MenuItem::add(MenuItem* item) {
	item->setMenu(menu);
	item->setFont(content->getFont());
	item->setScreen(content->getScreen());

	children.push_back(item);
	distributeChildren();
}

void MenuItem::remove(MenuItem* item) {
	std::vector<MenuItem*>::iterator i = children.begin();
	while(i != children.end()) {
		if((*i) == item) {
			i = children.erase(i);
		} else {
			i++;
		}
	}
	distributeChildren();
}

void MenuItem::distributeChildren() {
	if(content->getFont() == NULL) {
		return;
	}
	int lineHeight = TTF_FontHeight(content->getFont());
	int totalHeight = children.size() * lineHeight;
	// loop by index
	for(unsigned int idx = 0; idx < children.size(); idx++) {
		Label* label = children[idx]->content;
		const char* text = label->getCaption().c_str();
		// Find out the length of the text, in pixels
		int w = 0, h = 0;
		TTF_SizeUTF8(label->getFont(), text, &w, &h);
		// Align centered horizontally
		label->setX(menuBox.x + (menuBox.w - w) / 2);
		// Distribute vertically
		int skip = (menuBox.h - totalHeight) / (children.size() + 1);
		label->setY(menuBox.y + skip * (idx + 1) + lineHeight * idx);
	}
}

void MenuItem::translate() {
	setCaption(i18n(text));
	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->translate();
	}
}

void MenuItem::handleEvents(SDL_Event event) {
	if(!enabled) {
		return;
	}
	Button::handleEvents(event);
}

void MenuItem::handleChildrenEvents(SDL_Event event) {
	std::vector<MenuItem*>::iterator i;
	for(i = children.begin(); i != children.end(); i++) {
		(*i)->handleEvents(event);
	}
}

void MenuItem::mouseMove(SDL_MouseMotionEvent event) {
	if(isInside(event.x, event.y)) {
		// TODO do not change the color at every mouse move, as
		// this asks for rendering. It would be better to change
		// some state, and re-render only on state change.
		content->setColor(255, 0, 0);
	} else {
		content->setColor(0, 0, 0);
	}
}

void MenuItem::mouseDown(SDL_MouseButtonEvent event) {
}

void MenuItem::mouseUp(SDL_MouseButtonEvent event) {
	if(!isInside(event.x, event.y)) {
		return;
	}
	execute();
}

void MenuItem::execute() {
	if(children.size()) {
		menu->push(this);
	} else if(function1 != NULL) {
		function1(obj, arg); // make callback
	} else if(function0 != NULL) {
		function0(obj);
	}
	content->setColor(0, 0, 0);
}

