#include "guiUtils.h"
#include "SDL/SDL_image.h"

SDL_Surface *loadImage(std::string imgPath, std::string filename) {
	// Temporary storage for the image that's loaded
	SDL_Surface* loadedImage = NULL;

	// The optimized image that will be used
	SDL_Surface* optimizedImage = NULL;

	// Load the image
	filename = imgPath + filename;
	loadedImage = IMG_Load(filename.c_str());

	// If nothing went wrong in loading the image
	if(loadedImage != NULL) {
		// Create an optimized image
		optimizedImage = SDL_DisplayFormatAlpha(loadedImage);

		// Free the old image
		SDL_FreeSurface(loadedImage);
	}

	// Return the optimized image
	return optimizedImage;
}

void applySurface(
	int x, int y, SDL_Surface* source, SDL_Surface* destination,
	SDL_Rect* clip
) {
	// Make a temporary rectangle to hold the offsets
	SDL_Rect offset;

	// Give the offsets to the rectangle
	offset.x = x;
	offset.y = y;

	// Blit the surface
	SDL_BlitSurface(source, clip, destination, &offset);
}

SDL_Rect mkRect(int x, int y) {
	return mkRect(x, y, 0, 0);
}

SDL_Rect mkRect(int x, int y, int w, int h) {
	SDL_Rect rect;
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;
	return rect;
}

