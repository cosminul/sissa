#ifndef INPUT_MENU_ITEM_H
#define INPUT_MENU_ITEM_H

#include "menuItem.h"

class InputMenuItem : public MenuItem {
private:
	std::string input;

	int blink;

	Label* label;

	std::string labelText;

	/**
	 * Non-public member function that helps to avoid reduplicating
	 * identical pieces of contructors' code 
	 */
	void init();

public:
	InputMenuItem(
		std::string caption, TTF_Font* font, SDL_Rect box,
		SDL_Surface* screen, void* obj, void* arg,
		void (*function)(void* obj, void* arg)
	);

	InputMenuItem(
		std::string caption, void* obj, void* arg,
		void (*function)(void* obj, void* arg)
	);

	InputMenuItem(std::string);

	~InputMenuItem();

	void setMenu(Menu*);

	void setScreen(SDL_Surface*);

	void setFont(TTF_Font*);

	void setLabel(std::string);

	void keyPress(SDL_KeyboardEvent event);

	void handleEvents(SDL_Event event);

	void show();

	void translate();
};

#endif

