#ifndef INDICATOR_H
#define INDICATOR_H

#include "component.h"
#include "observer.h"
#include "observable.h"
#include "board.h"

class Indicator : public Component, public Observer {
private:
	Board* subject;

	SDL_Rect pos1;
	SDL_Rect pos2;

	SDL_Rect savedPos1;
	SDL_Rect savedPos2;

public:
	// The clips in the sprite sheet
	static const int CLIP_ON = 4;

	Indicator(
		Board* board, SDL_Rect box, SDL_Rect altBox, SDL_Surface* screen
	);

	~Indicator();

	void swapPositions();

	void resetPositions();

	void update(Observable* theChangedSubject);
};

#endif

