#ifndef GBOARD_H
#define GBOARD_H

#include "observer.h"
#include "observable.h"
#include "gSquare.h"
#include "board.h"
#include "label.h"

/**
 * GBoard - a graphical board. Actually, just a collection of
 * graphical squares.
 *
 * @date 2007 01 23
 */
class GBoard : public Component, public Observer {
private:
	/** A bidimensional array of pointers to GSquares */
	GSquare*** gSquares;

	/** A pointer to the selected square, or NULL if none is selected */
	GSquare* selectedSquare;

	/** A pointer to a real board */
	Board* board;

	Player* netPlayer;

	bool upsideDown;

public:
	static const int CLIP_BOARD = 0;
	static const int CLIP_FILES = 1;
	static const int CLIP_RANKS = 2;
	static const int CLIP_RFILES = 3; // reversed files
	static const int CLIP_RRANKS = 4; // reversed ranks

	GBoard(Player* netPlayer, SDL_Rect box, SDL_Surface* screen);

	~GBoard();

	/** Turn the board upside down */
	void flip();

	void setBlackSideDown();

	void setWhiteSideDown();

	bool isUpsideDown();

	void setPieceSprites(SDL_Surface* sheet, SDL_Rect* clips);

	void setSquareSprites(SDL_Surface* sheet, SDL_Rect* clips);

	void setScreen(SDL_Surface* screen);

	void init();

	void paintPieces();

	/**
	 * @return a pointer to the real board
	 */
	Board* getBoard();

	GSquare* getGSquare(char file, int rank);

	GSquare* getSelectedSquare();

	void setSelectedSquare(GSquare* gSquare);

	bool isInside(int x, int y);

	/** Handles events */
	void handleEvents(SDL_Event event);

	void showValidMoves(GSquare* gSquare);

	/** Unselects all squares */
	void unselectAll();

	/** Unselects all squares but one */
	void unselectAllBut(const GSquare* except);

	void show();

	void update(Observable* theChangedSubject);
};

#endif

