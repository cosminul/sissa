#include "gSquare.h"
#include "gBoard.h"
#include "guiUtils.h"
#include "sissa.h"

GSquare::GSquare(
	GBoard* gBoard, Square* square, SDL_Rect box, SDL_Surface* screen
):Button(box, screen) {
	this->gBoard = gBoard;
	this->square = square;

	pClips = NULL;

	// Set the default sprite
	pClip = NULL;

	selected = false;
}

GSquare::~GSquare() {
	// TODO bye bye, gsquare
}

void GSquare::setBox(SDL_Rect box) {
	this->box = box;	
}

Square* GSquare::getSquare() {
	return square;
}

std::string GSquare::getPosition() {
	return square->toString();
}

void GSquare::setPiece(int piece) {
	if(piece > -1) {
		pClip = &pClips[piece];
	} else {
		pClip = NULL;
	}
}

void GSquare::setPieceSheet(SDL_Surface* sheet) {
	pieceSheet = sheet;
}

void GSquare::setPieceClips(SDL_Rect* clips) {
	this->pClips = clips;
}

bool GSquare::isSelected() {
	return selected;
}

void GSquare::setSelected(bool selected) {
	if(square->getPiece() == NULL) {
		clip = NULL;
		return;
	}
	this->selected = selected;
	if(selected) {
		// Only allow picking up own pieces
		if(!square->getPiece()->getOwner()->isCurrentPlayer()) {
			clip = NULL;
			return;
		}
		clip = &clips[CLIP_MOUSEUP];
		gBoard->setSelectedSquare(this);
		gBoard->unselectAllBut(this);
	} else {
		clip = NULL;
		if(this == gBoard->getSelectedSquare()) {
			gBoard->setSelectedSquare(NULL);
		}
	}
}

void GSquare::showValidMoves() {
	// Only allow populated squares
	if(square->getPiece() == NULL) {
		return;
	}
	// Only allow picking up own pieces
	if(!square->getPiece()->getOwner()->isCurrentPlayer()) {
		return;
	}
	setSelected(true);
	gBoard->showValidMoves(this);
}

void GSquare::markAsValid() {
	clip = &clips[CLIP_VALID];
	if(square->getPiece() != NULL) {
		clip = &clips[CLIP_THREAT];
	}
}

void GSquare::selectOrMove() {
	GSquare* current = gBoard->getSelectedSquare();
	if(current == NULL) {
		// If there is no selected square, select this one
		setSelected(true);
	} else if(current == this) {
		// If this is the selected square, unselect it.
		// We also need to unselect the `valid moves' squares, so
		// better unselect everything.
		gBoard->unselectAll();
	} else if(square->isFriendOf(current->getSquare())) {
		setSelected(true);
	} else {
		// If some other square is selected, and there is
		// nothing on this one, try to move there
		char file = square->getFile();
		int rank = square->getRank();
		bool mv = current->getSquare()->getPiece()->move(file, rank);
		if(mv) {
			// no longer needed, as gBoard is an observer
			//!gBoard->paintPieces();
			gBoard->setSelectedSquare(NULL);
			gBoard->unselectAll();
		} else {
			gBoard->unselectAllBut(gBoard->getSelectedSquare());
		}
	}
}

void GSquare::mouseUp(SDL_MouseButtonEvent event) {
	// If the mouse is over the button
	if(isInside(event.x, event.y)) {
		// Set the square sprite
		if(event.button == SDL_BUTTON_LEFT) {
			selectOrMove();
			//!setSelected(!selected);
		} else if(event.button == SDL_BUTTON_RIGHT) {
			gBoard->unselectAllBut(this);
			showValidMoves();
		}
	}
}

void GSquare::mouseDown(SDL_MouseButtonEvent event) {
	// If the left mouse button was pressed
	if(event.button == SDL_BUTTON_LEFT) {
		// If the mouse is over the square
		if(isInside(event.x, event.y)) {
			// Set the square sprite
			clip = &clips[CLIP_MOUSEDOWN];
		}
	}
}

void GSquare::show() {
	if(pClip != NULL) {
		// Show the square's content
		int x = box.x + BORDER_WIDTH;
		int y = box.y + BORDER_WIDTH;
		applySurface(x, y, pieceSheet, screen, pClip);
	}
	if(clip != NULL) {
		// Show the square's border
		applySurface(box.x, box.y, spriteSheet, screen, clip);
	}
}

