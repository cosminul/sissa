#include "menu.h"
#include <iostream>

Menu::Menu(
	Board* board, Player* owner,
	TTF_Font** fonts, SDL_Rect box, SDL_Surface* screen
):Component(box, screen) {
	subject = board;
	subject->attach(this);

	this->owner = owner;

	this->fonts[FONT_NORMAL] = fonts[FONT_NORMAL];
	this->fonts[FONT_HIGHLIGHT] = fonts[FONT_HIGHLIGHT];
	this->fonts[FONT_LABEL] = fonts[FONT_LABEL];

	font = fonts[FONT_NORMAL];
	root = NULL;
	roots.reserve(3);
}

Menu::~Menu() {
	subject->detach(this);
}

TTF_Font* Menu::getFont(int index) {
	return fonts[index];
}

bool Menu::isEmpty() {
	return items.empty();
}

void Menu::empty() {
	while(!items.empty()) {
		items.pop();
	}
}

void Menu::reset() {
	empty();
	push(root);
}

void Menu::push(MenuItem* item) {
	items.push(item);	
}

MenuItem* Menu::pop() {
	MenuItem* item = items.top();
	items.pop();
	return item;
}

MenuItem* Menu::top() {
	return items.top();
}

char* Menu::getResult() {
	return result;
}

void Menu::setResult(const char* result) {
//!	this->result = result; // not good, because it moves the pointer
	strcpy(this->result, result);
}

// Static wrapper function to be able to callback the member function pop()
void Menu::cmdGoBack(void* object) {
	// Explicitly cast to a pointer to Menu
	Menu* mySelf = (Menu*)object;
	// Call member
	mySelf->pop();
}

void Menu::addRoot(MenuItem* root) {
	roots.push_back(root);
}

void Menu::setRootMenuItem(MenuItem* item) {
	item->setMenu(this);
	item->setFont(font);
	item->setScreen(screen);

	root = item;
	reset();
}

void Menu::setRootMenuItem(std::string rootName) {
	std::vector<MenuItem*>::iterator i;
	for(i = roots.begin(); i != roots.end(); i++) {
		if((*i)->getCaption() == rootName) {
			setRootMenuItem(*i);
		}
	}
}

void Menu::translate() {
	std::vector<MenuItem*>::iterator i;
	for(i = roots.begin(); i != roots.end(); i++) {
		(*i)->translate();
	}
}

void Menu::handleEvents(SDL_Event event) {
	items.top()->handleChildrenEvents(event);
}

void Menu::update(Observable* theChangedSubject) {
	if(theChangedSubject != subject) {
		return;
	}
	if(subject->getMessage() != Board::MSG_PROMOTE) {
		return;
	}
	if(subject->getCurrent() == owner) {
		// the owner has already changed, right after the move
		return;
	}
	setRootMenuItem("$prom_root");
	visible = true;
}

void Menu::show() {
	if(visible == false) {
		return;
	}
	Component::show();
	items.top()->showChildren();
}

