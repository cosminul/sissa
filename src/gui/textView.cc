#include "textView.h"

TextView::TextView(
	std::string caption, TTF_Font* font, SDL_Rect box, SDL_Surface* screen
):Label(caption, font, box, screen) {
	lineSkip = TTF_FontLineSkip(font);
	setCaption(caption);
}

TextView::~TextView() {
}

void TextView::setCaption(std::string caption) {
	this->caption = caption;
	lines = split(caption);
	width = box.w;
	height = box.h;
	computeDimensions();
	render();
}

void TextView::render() {
	SDL_Color fg = textColor;
	SDL_Color bg = background;
	SDL_FreeSurface(spriteSheet);

	// Make the surface to which to blit the text:
	spriteSheet = SDL_CreateRGBSurface(SDL_SWSURFACE, width, height,
		screen->format->BitsPerPixel,
		screen->format->Rmask,
		screen->format->Gmask,
		screen->format->Bmask,
		screen->format->Amask
	);
	// (it's compatible with the destination Surface
	
	// Now, fill it with the background color:
	Uint32 color = SDL_MapRGB(screen->format, bg.r, bg.g, bg.b);
	SDL_FillRect(spriteSheet, &spriteSheet->clip_rect, color);

	// Actually render the text
	SDL_Surface* sTemp = NULL;
	for(unsigned int i = 0; i < lines.size(); i++) {
		// The rendered text:
		sTemp = TTF_RenderUTF8_Shaded(font, lines[i].c_str(), fg, bg);
		// Put it on the surface (sText)
		SDL_Rect offset;
		offset.x = 0; offset.y = i * lineSkip;
		SDL_BlitSurface(sTemp, NULL, spriteSheet, &offset);
		// Clean up
		SDL_FreeSurface(sTemp);
	}
}

std::vector<std::string> TextView::split(std::string str) {
	std::vector<std::string> vLines; // these are the individual lines
	vLines.reserve(1);
	int n = 0;
	while(n != -1) {
		// Get until either '\n' or '\0':
		std::string strSub;
		n = str.find('\n', 0); // Find the next '\n'
		strSub = str.substr(0, n);
		if(n != -1) {
			str = str.substr(n + 1, -1);
		}
		vLines.push_back(strSub);
	}
	return vLines;
}

void TextView::computeDimensions() {
	std::vector<std::string>::iterator i;
	int h = 0;
	for(i = lines.begin(); i != lines.end(); i++) {
		int w = 0;
		TTF_SizeText(font, (*i).c_str(), &w, &h);
		if(w > width) {
			width = w;
		}
	}
	// height = number of lines-and-spaces-before-them, plus the 1st line
	int totalH = (lines.size() - 1) * lineSkip + h;
	if(totalH > height) {
		height = totalH;
	}
}

