#ifndef TEXT_VIEW_H
#define TEXT_VIEW_H

#include <string>
#include <vector>
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "label.h"

/**
 * Text view - a widget that is able to display multiple lines
 * of text on an SDL_Surface.
 * @author Cosmin
 * @date 2007 04 16
 */
class TextView : public Label {
private:
	int width;

	int height;

	int lineSkip;

	std::vector<std::string> lines;

	/**
	 * Breaks a string into its lines
	 * @param str a multi-line string
	 * @return a vector of lines
	 */
	std::vector<std::string> split(std::string str);

	void computeDimensions();

	void render();

public:
	TextView(
		std::string caption, TTF_Font* font,
		SDL_Rect box, SDL_Surface* screen
	);

	~TextView();

	void setCaption(std::string caption);

};

#endif
