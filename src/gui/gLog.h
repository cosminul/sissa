#ifndef GLOG_H
#define GLOG_H

#include "observer.h"
#include "board.h"
#include "component.h"
#include "label.h"
#include "SDL/SDL_ttf.h"

class GLog : public Component, public Observer {
private:
	Label* content;

	Board* subject;

public:
	GLog(Board* board, TTF_Font* font, SDL_Rect box, SDL_Surface* screen);

	~GLog();

	void update(Observable* theChangedSubject);

	void setScreen(SDL_Surface* screen);

	void show();
};

#endif

