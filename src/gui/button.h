#ifndef BUTTON_H
#define BUTTON_H

#include "SDL/SDL.h"
#include "component.h"

/**
 * Button - a clickable Component.
 * @author Cosmin
 * @date 2007 03 10
 */

class Button : public Component {
protected:
	/**
	 * The this-pointer of a class object. Can be used for a callback to
	 * a non-static member of an arbitrary class.
	 */
	void* obj;

	/**
	 * A generic argument to be passed to the callback function
	 */
	void* arg;

	/**
	 * A non-static member function of a class, with no arguments
	 */
	void (*function0)(void* obj);

	/**
	 * A non-static member function of a class, with one argument
	 */
	void (*function1)(void* obj, void* arg);

	virtual void mouseMove(SDL_MouseMotionEvent event);

	virtual void mouseDown(SDL_MouseButtonEvent event);

	virtual void mouseUp(SDL_MouseButtonEvent event);

	/**
	 * This constructor is protected because it does not define the
	 * box and the screen of the component. It will only be used by
	 * descendants.
	 */
	Button();

private:
	/**
	 * Non-public member function that helps to avoid reduplicating
	 * identical pieces of contructors' code 
	 */
	void init();

public:
	// The button states in the sprite sheet
	static const int CLIP_MOUSEDOWN = 0;
	static const int CLIP_MOUSEUP = 1;

	// The width of the border, in pixels
	static const int BORDER_WIDTH = 2;

	/**
	 * Initialize the variables
	 */
	Button(SDL_Rect box, SDL_Surface* screen);

	Button(
		SDL_Rect box, SDL_Surface* screen,
		void* obj, void (*function)(void* obj)
	);

	Button(
		SDL_Rect box, SDL_Surface* screen,
		void* obj, void* arg, void (*function)(void* obj, void* arg)
	);

	virtual ~Button();

	/** Tells whether the some coordinates are inside the button */
	virtual bool isInside(int x, int y);

	/** Handles events and set the box's sprite region */
	void handleEvents(SDL_Event event);
};

#endif

