#include "gui.h"
#include "netGame.h"
#include "inputMenuItem.h"
#include "nameLabel.h"
#include "lang.h"
#include <iostream>

int promotion[4];
int langs[2];

Gui::Gui() {
	screenWidth = 640;
	screenHeight = 480;
	screenBPP = 32; // bits per-pixel
	imgPath = "../img/";
	pieces = NULL;
	background = NULL;
	screen = NULL;
	squares = NULL;
	buttons = NULL;
	font = NULL;
	quit = false;
	state = STATE_RUN;
}

Gui::~Gui() {
}

SDL_Surface* Gui::getScreen() {
	return screen;
}

GBoard* Gui::getGBoard() {
	return board;
}

Indicator* Gui::getCurPlayer() {
	return curPlayer;
}

Menu* Gui::getMenu() {
	return menu;
}

bool Gui::canQuit() {
	return quit;
}

bool Gui::init() {
	// Initialize the SDL video subsystem
	if(SDL_Init(SDL_INIT_VIDEO) == -1) {
		return false;
	}
	if(TTF_Init() == -1) {
		return false;
	}
	if(SDLNet_Init() == -1) {
		return false;
	}

	// Sets the window icon
	SDL_WM_SetIcon(IMG_Load("../img/icon.png"), NULL);

	// Set up the screen
	fullscreen = false;
	screen = SDL_SetVideoMode(
			screenWidth, screenHeight, screenBPP, SDL_SWSURFACE
	);

	// If there was an error in setting up the screen
	if(screen == NULL) {
		return false;
	}

	// Set the window caption
	SDL_WM_SetCaption("Sissa", NULL);

	// Enable unicode; needed for keypress events
	SDL_EnableUNICODE(1);

	// If everything initialized fine
	return true;
}

bool Gui::loadFiles() {
	// Load the images
	pieces = loadImage(imgPath, "sprites.png");
	background = loadImage(imgPath, "board.png");
	squares = loadImage(imgPath, "square_borders.png");
	buttons = loadImage(imgPath, "buttons.png");
	std::string fontName = (imgPath + "VeraSe.ttf").c_str();
	font = TTF_OpenFont(fontName.c_str(), 14);
	menuFonts[Menu::FONT_NORMAL] = TTF_OpenFont(fontName.c_str(), 24);
	menuFonts[Menu::FONT_HIGHLIGHT] = TTF_OpenFont(fontName.c_str(), 26);
	menuFonts[Menu::FONT_LABEL] = TTF_OpenFont(fontName.c_str(), 12);

	// If there was an error in loading the images
	if(
			pieces == NULL || background == NULL
			|| squares == NULL || buttons == NULL
	) {
		std::cerr << "Error loading image file." << std::endl;
		return false;
	}

	if(font == NULL) {
		std::cerr << "Error loading font file." << std::endl;
		return false;
	}

	// If everything loaded fine
	return true;
}

void Gui::defineClips() {
	defineBoardClips();
	definePieceClips();
	defineSquareClips();
	defineButtonClips();
}

void Gui::definePieceClips() {
	clip[1] = mkRect(1, 1, 40, 40); // black king
	clip[3] = mkRect(42, 1, 40, 40); // black queen
	clip[5] = mkRect(83, 1, 40, 40); // black rook
	clip[7] = mkRect(124, 1, 40, 40); // black bishop
	clip[9] = mkRect(165, 1, 40, 40); // black pawn
	clip[11] = mkRect(206, 1, 40, 40); // black knight

	clip[0] = mkRect(1, 42, 40, 40); // white king
	clip[2] = mkRect(42, 42, 40, 40); // white queen
	clip[4] = mkRect(83, 42, 40, 40); // white rook
	clip[6] = mkRect(124, 42, 40, 40); // white bishop
	clip[8] = mkRect(165, 42, 40, 40); // white pawn
	clip[10] = mkRect(206, 42, 40, 40); // white knight
}

void Gui::defineSquareClips() {
	sqClip[0] = mkRect(1, 1, 44, 44);
	sqClip[1] = mkRect(46, 1, 44, 44);
	sqClip[2] = mkRect(91, 1, 44, 44);
	sqClip[3] = mkRect(136, 1, 44, 44);
}

void Gui::defineButtonClips() {
	btClip[0] = mkRect(210, 32, 88, 30);
	btClip[1] = mkRect(210, 1, 88, 30);
	btClip[4] = mkRect(179, 1, 30, 30);
	btClip[5] = mkRect(1, 63, 320, 240);
	btClip[6] = mkRect(1, 304, 320, 60);
}

void Gui::defineBoardClips() {
	boardClip[GBoard::CLIP_BOARD] = mkRect(20, 0, 338, 338);
	boardClip[GBoard::CLIP_FILES] = mkRect(20, 338, 338, 17);
	boardClip[GBoard::CLIP_RANKS] = mkRect(10, 0, 10, 338);
	boardClip[GBoard::CLIP_RFILES] = mkRect(20, 355, 338, 17);
	boardClip[GBoard::CLIP_RRANKS] = mkRect(0, 0, 10, 338);
}

void Gui::toggleFullscreen() {
	fullscreen = !fullscreen;
	Uint32 screenMode = fullscreen ? SDL_FULLSCREEN : SDL_SWSURFACE;
	SDL_FreeSurface(screen);
	screen = SDL_SetVideoMode(
			screenWidth, screenHeight, screenBPP, screenMode
	);
	board->setScreen(screen);
	log->setScreen(screen);
	menuButton->setScreen(screen);
	downName->setScreen(screen);
	upName->setScreen(screen);
	curPlayer->setScreen(screen);
	menu->setScreen(screen);
}

// Static wrapper function to be able to callback
// the member function toggleFullscreen()
void Gui::cmdToggleFullscreen(void* object) {
	// Explicitly cast to a pointer to Gui
	Gui* mySelf = (Gui*)object;
	// Call member
	mySelf->toggleFullscreen();
}

void Gui::cmdToggleMenu(void* object) {
	Gui* mySelf = (Gui*)object;
	mySelf->toggleMenu();
}

void Gui::cmdExit(void* object) {
	Gui* mySelf = (Gui*)object;
	mySelf->quit = true;
}

void Gui::cmdHost(void* object) {
	Gui* mySelf = (Gui*)object;
	mySelf->netGame->host();
	// Reset and hide the menu
	mySelf->toggleMenu();

	mySelf->board->setWhiteSideDown();
	mySelf->curPlayer->resetPositions();
	mySelf->curPlayer->setVisible(true);
}

void Gui::cmdJoin(void* object, void* arg) {
	Gui* mySelf = (Gui*)object;

	char* ip = (char*)arg;

	mySelf->netGame->join(ip);
	// Reset and hide the menu
	mySelf->toggleMenu();

	// Bring the black pieces closer
	mySelf->board->setBlackSideDown();
	
	mySelf->curPlayer->resetPositions();
	mySelf->curPlayer->swapPositions();
	mySelf->curPlayer->setVisible(true);
}

void Gui::cmdDraw(void* object) {
	Gui* mySelf = (Gui*)object;

	char msg[16];
	strcpy(msg, "d");

	mySelf->netGame->send(msg);
	// Reset and hide the menu
	mySelf->toggleMenu();
}

void Gui::cmdAcceptDraw(void* object) {
	Gui* mySelf = (Gui*)object;

	char msg[16];
	strcpy(msg, "y!"); // `!' forces disconnect after sending the message

	mySelf->netGame->send(msg);
	// Reset and hide the menu
	mySelf->toggleMenu();
	mySelf->menu->setRootMenuItem("$main_root");
}

void Gui::cmdDeclineDraw(void* object) {
	Gui* mySelf = (Gui*)object;

	char msg[16];
	strcpy(msg, "n");

	mySelf->netGame->send(msg);
	// Reset and hide the menu
	mySelf->toggleMenu();
	mySelf->menu->setRootMenuItem("$main_root");
}

void Gui::cmdResign(void* object) {
	Gui* mySelf = (Gui*)object;

	char msg[16];
	strcpy(msg, "r!"); // `!' forces disconnect after sending the message

	mySelf->netGame->send(msg);
	// Reset and hide the menu
	mySelf->toggleMenu();
}

void Gui::cmdSetPlayer(void* object, void* arg) {
	Gui* mySelf = (Gui*)object;
	char* name = (char*)arg;

	mySelf->player->setName(name);
//!	// Reset and hide the menu
//!	mySelf->toggleMenu();
	mySelf->menu->setRootMenuItem("$main_root");
}

void Gui::cmdPromote(void* object, void* arg) {
	Gui* mySelf = (Gui*)object;
	int identity = *((int*)arg);
	Pawn* pawn = (Pawn*)mySelf->board->getBoard()->getMessageData();
	pawn->promote(identity);

	// Reset and hide the menu
	mySelf->toggleMenu();
	mySelf->menu->setRootMenuItem("$main_root");
}

void Gui::cmdChLang(void* object, void* arg) {
	Gui* mySelf = (Gui*)object;
	int language = *((int*)arg);
	Lang::getInstance()->load(language);

	mySelf->menu->translate();
}

void Gui::cmdFlipBoard(void* object) {
	Gui* mySelf = (Gui*)object;

	mySelf->board->flip();
	mySelf->curPlayer->swapPositions();

	Player* aux = mySelf->downName->getPlayer();
	mySelf->downName->setPlayer(mySelf->upName->getPlayer());
	mySelf->upName->setPlayer(aux);
}

void Gui::toggleMenu() {
	if(state == STATE_RUN) {
		menu->reset();
		state = STATE_MENU;
		menu->setVisible(true);
	} else if(state == STATE_MENU) {
		state = STATE_RUN;
		menu->setVisible(false);
	}
}

void Gui::cleanUp() {
	cleanUpGameComponents();

	// Free the surfaces
	SDL_FreeSurface(pieces);
	SDL_FreeSurface(squares);
	SDL_FreeSurface(background);
	SDL_FreeSurface(screen);

	TTF_CloseFont(font);

	// Quit SDL_net
	SDLNet_Quit();
	// Quit SDL_ttf
	TTF_Quit();
	// Quit SDL
	SDL_Quit();
}

void Gui::handleEvent(SDL_Event event) {
	// If the user has Xed out the window
	if(event.type == SDL_QUIT) {
		// Quit the program
		quit = true;
	}

	if(state == STATE_MENU) {
		menu->handleEvents(event);
		return;
	}
	// Handle board events
	if(netGame->isConnected()) {
		board->handleEvents(event);
	}
	// Handle button events
	menuButton->handleEvents(event);

	if(event.type == SDL_KEYDOWN) {
		switch(event.key.keysym.sym) {
		case SDLK_f:
			toggleFullscreen();
			break;
		case SDLK_q:
			quit = true;
			break;
		case SDLK_ESCAPE:
			toggleMenu();
			break;
		default:
			break; // Nothing to do
		}
	}
}

void Gui::setupGameComponents() {
	player = new Player("Player 1");
	opponent = new Player("Player 2");

	setupBoard();

	netGame = new NetGame(this, player);

	setupButtons();
	setupLabels();
	setupMenu();

	// Ask for the player's name
	menu->setRootMenuItem("$name_root");
	state = STATE_MENU;
}

void Gui::setupBoard() {
	board = new GBoard(player, mkRect(50, 65, 338, 338), screen);
	board->setSpriteSheet(background);
	board->setClips(boardClip);
	board->setPieceSprites(pieces, clip);
	board->setSquareSprites(squares, sqClip);
	board->init();

	SDL_Rect logBox = mkRect(439, 71, 151, 338);
	log = new GLog(board->getBoard(), font, logBox, screen);

	SDL_Rect pos1 = mkRect(145, 423, 30, 30);
	SDL_Rect pos2 = mkRect(145, 27, 30, 30);
	curPlayer = new Indicator(board->getBoard(), pos1, pos2, screen);
	curPlayer->setSpriteSheet(buttons);
	curPlayer->setClips(btClip, &btClip[Indicator::CLIP_ON]);
	curPlayer->setVisible(false);
}

void Gui::setupButtons() {
	SDL_Rect box = mkRect(50, 27, 88, 30);
	menuButton = new Button(box, screen, this, cmdToggleMenu);
	menuButton->setSpriteSheet(buttons);
	menuButton->setClips(btClip, &btClip[1]);
}

void Gui::setupLabels() {
	Board* brd = board->getBoard();

	downName = new NameLabel(brd, netGame->getPlayer(), font, mkRect(185, 430), screen);
	downName->setColor(255, 255, 255);
	downName->setBackground(68, 52, 0);

	upName = new NameLabel(brd, netGame->getOpponent(), font, mkRect(185, 34), screen);
	upName->setColor(255, 255, 255);
	upName->setBackground(68, 52, 0);

	message = new MessageBox(brd, netGame->getPlayer(), font, mkRect(160, 210, 320, 60), screen);
	message->setSpriteSheet(buttons);
	message->setClips(btClip, &btClip[6]);
}

void Gui::setupMenu() {
	Board* brd = board->getBoard();
	SDL_Rect box = mkRect(160, 120, 320, 240);

	menu = new Menu(brd, player, menuFonts, box, screen);
	menu->setSpriteSheet(buttons);
	menu->setClips(btClip, &btClip[5]);

	setupMainMenu();
	setupNameMenu();
	setupDrawMenu();
	setupPromoteMenu();

	menu->setRootMenuItem("$main_root");
}

void Gui::setupMainMenu() {
	MenuItem* rootMenu = new MenuItem("$main_root");

	newMenu = new MenuItem(i18n("New game"));
	rootMenu->add(newMenu);
	newMenu->add(new MenuItem(i18n("Host"), this, cmdHost));
	MenuItem* joinMenu = new MenuItem(i18n("Join"));
	newMenu->add(joinMenu);
	MenuItem* hostName = new InputMenuItem("127.0.0.1", this, menu->getResult(), cmdJoin);
	((InputMenuItem*)hostName)->setLabel(i18n("Connect to host:"));
	joinMenu->add(hostName);
	joinMenu->add(new MenuItem(i18n("Back"), menu, Menu::cmdGoBack));
	newMenu->add(new MenuItem(i18n("Back"), menu, Menu::cmdGoBack));

	endMenu = new MenuItem(i18n("End game"));
	rootMenu->add(endMenu);
	endMenu->add(new MenuItem(i18n("Offer draw"), this, cmdDraw));
	endMenu->add(new MenuItem(i18n("Resign"), this, cmdResign));
	endMenu->add(new MenuItem(i18n("Back"), menu, Menu::cmdGoBack));

	MenuItem* optMenu = new MenuItem(i18n("Options"));
	rootMenu->add(optMenu);
	optMenu->add(new MenuItem(i18n("Toggle fullscreen"), this, cmdToggleFullscreen));
	optMenu->add(new MenuItem(i18n("Flip board"), this, cmdFlipBoard));
	MenuItem* langMenu = new MenuItem(i18n("Language"));
	optMenu->add(langMenu);
	langs[0] = Lang::LANG_EN;
	void* arg0 = (void*)&langs[0];
	langs[1] = Lang::LANG_RO;
	void* arg1 = (void*)&langs[1];
	langMenu->add(new MenuItem(i18n("English"), this, arg0, cmdChLang));
	langMenu->add(new MenuItem(i18n("Romana"), this, arg1, cmdChLang));
	langMenu->add(new MenuItem(i18n("Back"), menu, Menu::cmdGoBack));
	optMenu->add(new MenuItem(i18n("Back"), menu, Menu::cmdGoBack));

	rootMenu->add(new MenuItem(i18n("Continue"), menu, Menu::cmdGoBack));
	rootMenu->add(new MenuItem(i18n("Exit"), this, cmdExit));

	menu->addRoot(rootMenu);
}

void Gui::setupNameMenu() {
	MenuItem* rootMenu = new MenuItem("$name_root");

	MenuItem* playerName =
		new InputMenuItem("", this, menu->getResult(), cmdSetPlayer);
	((InputMenuItem*)playerName)->setLabel(i18n("Your name:"));
	rootMenu->add(playerName);

	menu->addRoot(rootMenu);
}

void Gui::setupDrawMenu() {
	MenuItem* rootMenu = new MenuItem("$draw_root");

	MenuItem* label = new MenuItem(i18n("Opponent offers draw"));
	label->setEnabled(false);
	rootMenu->add(label);
	rootMenu->add(new MenuItem(i18n("Accept"), this, cmdAcceptDraw));
	rootMenu->add(new MenuItem(i18n("Decline"), this, cmdDeclineDraw));

	menu->addRoot(rootMenu);
}

void Gui::setupPromoteMenu() {
	MenuItem* rootMenu = new MenuItem("$prom_root");

	/* The following code is a mess because of the public constants from
	 * `Pawn'. We can't just say &Pawn::CONSTANT because this would make
	 * a pointer to some data in a library. So we use this `promotion'
	 * variable, which needs to be global in order to be seen in
	 * `cmdPromote()'.
	 */
	promotion[0] = Pawn::IDENTITY_QUEEN;
	void* arg0 = (void*)&promotion[0];
	promotion[1] = Pawn::IDENTITY_ROOK;
	void* arg1 = (void*)&promotion[1];
	promotion[2] = Pawn::IDENTITY_KNIGHT;
	void* arg2 = (void*)&promotion[2];
	promotion[3] = Pawn::IDENTITY_BISHOP;
	void* arg3 = (void*)&promotion[3];

	MenuItem* queen = new MenuItem(i18n("Queen"), this, arg0, cmdPromote);
	MenuItem* rook = new MenuItem(i18n("Rook"), this, arg1, cmdPromote);
	MenuItem* knight = new MenuItem(i18n("Knight"), this, arg2, cmdPromote);
	MenuItem* bishop = new MenuItem(i18n("Bishop"), this, arg3, cmdPromote);

	rootMenu->add(queen);
	rootMenu->add(rook);
	rootMenu->add(knight);
	rootMenu->add(bishop);

	menu->addRoot(rootMenu);
}

void Gui::cleanUpGameComponents() {
	delete board;
	delete menuButton;
	delete downName;
	delete upName;
	delete curPlayer;
	delete menu;
	delete message;
}

void Gui::showGameComponents() {
	// Fill the screen with some color
	Uint32 color = SDL_MapRGB(screen->format, 0x44, 0x34, 0x00);
	SDL_FillRect(screen, &screen->clip_rect, color);

	board->show();
	log->show();
	menuButton->show();
	downName->show();
	upName->show();
	curPlayer->show();
	message->show(); // this is invisible most of the time
	// Hide the menu if empty
	if(state == STATE_MENU && menu->isEmpty()) {
		toggleMenu();
	}
	// Switch to menu state if the menu made itself visible
	if(state != STATE_MENU && menu->isVisible()) {
		state = STATE_MENU;
	}
	if(netGame->isConnected()) {
		newMenu->setEnabled(false);
		endMenu->setEnabled(true);
	} else {
		newMenu->setEnabled(! netGame->isReady());
		endMenu->setEnabled(false);
	}
	menu->show();
}

