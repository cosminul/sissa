#include "label.h"
#include <iostream>

Label::Label(std::string caption):Component() {
	this->caption = caption;
	font = NULL;
	setColor(0, 0, 0);
	setBackground(255, 255, 255);
	quality = SHADED;
}

Label::Label(std::string capt, TTF_Font* font, SDL_Rect box, SDL_Surface* scr)
:Component(box, scr) {
	this->caption = capt;
	this->font = font;
	setColor(0, 0, 0);
	setBackground(255, 255, 255);
	quality = SHADED;
	render();
}

Label::~Label() {
}

std::string Label::getCaption() {
	return caption;
}

void Label::setCaption(std::string caption) {
	this->caption = caption;
	render();
}

int Label::getX() {
	return box.x;
}

void Label::setX(int x) {
	box.x = x;
	render();
}

int Label::getY() {
	return box.y;
}

void Label::setY(int y) {
	box.y = y;
	render();
}

void Label::setPosition(int x, int y) {
	box.x = x;
	box.y = y;
	render();
}

void Label::setColor(int red, int green, int blue) {
	textColor.r = red;
	textColor.g = green;
	textColor.b = blue;
	render();
}

void Label::setBackground(int red, int green, int blue) {
	background.r = red;
	background.g = green;
	background.b = blue;
	render();
}

TTF_Font* Label::getFont() {
	return font;
}

void Label::setFont(TTF_Font* font) {
	this->font = font;
	render();
}

void Label::setQuality(int quality) {
	this->quality = quality;
}

void Label::render() {
	if(font == NULL) {
		return;
	}
	SDL_Color fg = textColor;
	SDL_Color bg = background;
	SDL_FreeSurface(spriteSheet);
	const char* text = caption.c_str();
	switch(quality) {
	case SHADED:
		spriteSheet = TTF_RenderUTF8_Shaded(font, text, fg, bg);
		break;
	case BLENDED:
		spriteSheet = TTF_RenderUTF8_Blended(font, text, fg);
		break;
	default:
		spriteSheet = TTF_RenderUTF8_Solid(font, text, fg);
	}
	// Update box information
	if(box.w == 0) {
		int w, h = 0;
		TTF_SizeUTF8(font, text, &w, &h);
		box.w = w;
		box.h = h;
	}
}

