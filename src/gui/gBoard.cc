#include "piece.h"
#include "gBoard.h"
#include "gSquare.h"
#include "guiUtils.h"
#include "textView.h"
#include "util.h"
#include <string>

GBoard::GBoard(Player* netPlayer, SDL_Rect box, SDL_Surface* screen)
:Component(box, screen) {
	this->netPlayer = netPlayer;
	board = new Board;
	selectedSquare = NULL;
	gSquares = new GSquare**[8];
	for(unsigned char file = 0; file < 8; file++) {
		gSquares[file] = new GSquare*[8];
		for(int rank = 0; rank < 8; rank++) {
			int x = box.x + file * (44 - GSquare::BORDER_WIDTH);
			int y = box.y + (7-rank) * (44 - GSquare::BORDER_WIDTH);
			Square* sq = board->getSquare(file + 'a', rank + 1);
			SDL_Rect sqBox = mkRect(x, y, 44, 44);
			gSquares[file][rank] = new GSquare(this, sq, sqBox, NULL);
		}
	}
	upsideDown = false;
	board->attach(this);
}

GBoard::~GBoard() {
	board->detach(this);
	// TODO first, delete each square
//!	delete board;
}

void GBoard::flip() {
	if(upsideDown) {
		setWhiteSideDown();
	} else {
		setBlackSideDown();
	}
}

void GBoard::setWhiteSideDown() {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			int x = box.x + file * (44 - GSquare::BORDER_WIDTH);
			int y = box.y + (7-rank) * (44 - GSquare::BORDER_WIDTH);
			SDL_Rect sqBox = mkRect(x, y, 44, 44);
			gSquares[file][rank]->setBox(sqBox);
		}
	}
	upsideDown = false;
}

void GBoard::setBlackSideDown() {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			int x = box.x + (7-file) * (44 - GSquare::BORDER_WIDTH);
			int y = box.y + rank * (44 - GSquare::BORDER_WIDTH);
			SDL_Rect sqBox = mkRect(x, y, 44, 44);
			gSquares[file][rank]->setBox(sqBox);
		}
	}
	upsideDown = true;
}

bool GBoard::isUpsideDown() {
	return upsideDown;
}

void GBoard::setPieceSprites(SDL_Surface* sheet, SDL_Rect* clips) {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->setPieceSheet(sheet);
			gSquares[file][rank]->setScreen(screen);
			gSquares[file][rank]->setPieceClips(clips);
		}
	}
}

void GBoard::setSquareSprites(SDL_Surface* sheet, SDL_Rect* clips) {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->setSpriteSheet(sheet);
			gSquares[file][rank]->setScreen(screen);
			gSquares[file][rank]->setClips(clips);
		}
	}
}

void GBoard::setScreen(SDL_Surface* screen) {
	this->screen = screen;
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->setScreen(screen);
		}
	}
}

void GBoard::init() {
	board->init();
	paintPieces();
}

void GBoard::paintPieces() {
	for(int rank = 0; rank < 8; rank++) {
		for(unsigned char file = 0; file < 8; file++) {
			Piece* piece = board->pickUp(file + 'a', rank + 1);
			int pieceNumber;
			if(piece != NULL) {
				pieceNumber = piece->getAppearance();
			} else {
				pieceNumber = -1;
			}
			gSquares[file][rank]->setPiece(pieceNumber);
		}
	}
}

Board* GBoard::getBoard() {
	return board;
}

GSquare* GBoard::getSelectedSquare() {
	return selectedSquare;
}

void GBoard::setSelectedSquare(GSquare* gSquare) {
	selectedSquare = gSquare;
}

GSquare* GBoard::getGSquare(char file, int rank) {
	return gSquares[file - 'a'][rank - 1];
}

bool GBoard::isInside(int x, int y) {
	bool over = true;
	over = over && (x > box.x);
	over = over && (x < box.x + box.w);
	over = over && (y > box.y);
	over = over && (y < box.y + box.h);
	return over;
}

void GBoard::handleEvents(SDL_Event event) {
	if(board->getCurrent() != netPlayer) {
		return;
	}
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->handleEvents(event);
		}
	}
}

void GBoard::showValidMoves(GSquare* gSquare) {
	std::vector<Square*> valid;
	if(gSquare->getSquare()->getPiece() == NULL) {
		return;
	}
	valid = gSquare->getSquare()->getPiece()->validMoves();
	std::vector<Square*>::iterator iter;
	for(iter = valid.begin(); iter != valid.end(); iter++) {
		char file = (*iter)->getFile();
		int rank = (*iter)->getRank();
		getGSquare(file, rank)->markAsValid();
	}
}

void GBoard::unselectAll() {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->setSelected(false);
		}
	}
}

void GBoard::unselectAllBut(const GSquare* except) {
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			if(gSquares[file][rank] != except) {
				gSquares[file][rank]->setSelected(false);
			}
		}
	}
}

void GBoard::show() {
	// Show the board with all its squares
	applySurface(box.x, box.y, spriteSheet, screen, &clips[CLIP_BOARD]);
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			gSquares[file][rank]->show();
		}
	}
	// Show the labels with files and ranks
	SDL_Rect fLabels, rLabels;
	if(upsideDown) {
		fLabels = clips[CLIP_RFILES];
		rLabels = clips[CLIP_RRANKS];
	} else {
		fLabels = clips[CLIP_FILES];
		rLabels = clips[CLIP_RANKS];
	}
	applySurface(box.x, box.y + box.h, spriteSheet, screen, &fLabels);
	applySurface(box.x - 10, box.y, spriteSheet, screen, &rLabels);
}

void GBoard::update(Observable* theChangedSubject) {
	if(theChangedSubject != board) {
		return;
	}
	paintPieces();
}

