#ifndef MESSAGE_BOX_H
#define MESSAGE_BOX_H

#include <string>
#include "observer.h"
#include "board.h"
#include "player.h"
#include "button.h"
#include "label.h"

class MessageBox : public Button, public Observer {
private:
	Label* content;

	Board* subject;

	Player* owner;

	/** When has the message become visible */
	Uint32 popTime;

	/** How long should the message stay on screen */
	Uint32 delay;

	void render();

	/**
	 * Non-public member function that helps to avoid reduplicating
	 * identical pieces of contructors' code 
	 */
	void init();

public:
	static const int DEFAULT_DELAY = 1500; // 1.5 seconds

	MessageBox(TTF_Font*, SDL_Rect, SDL_Surface*);

	MessageBox(
		Board* subject, Player* owner,
		TTF_Font* font, SDL_Rect box, SDL_Surface* screen
	);

	~MessageBox();

	std::string getCaption();

	void setCaption(std::string);

	void setScreen(SDL_Surface*);

	void setFont(TTF_Font*);

	Uint32 getDelay();

	void setDelay(Uint32 delay);

	void show();

	void pop(std::string);

	void update(Observable* theChangedSubject);
};

#endif

