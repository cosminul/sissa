#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include <SDL/SDL_net.h>
#include <string>
#include "guiUtils.h"
#include "gBoard.h"
#include "gLog.h"
#include "button.h"
#include "nameLabel.h"
#include "textView.h"
#include "indicator.h"
#include "menu.h"
#include "menuItem.h"
#include "messageBox.h"
//#include "netGame.h"
#include "sissa.h"

class NetGame;

class Gui {
private:
	static const int STATE_RUN = 0;
	static const int STATE_MENU = 1;

	int state;

	// The attributes of the screen
	int screenWidth;
	int screenHeight;
	int screenBPP; // bits per-pixel

	std::string imgPath;

	// The surfaces that will be used
	SDL_Surface *pieces;
	SDL_Surface *background;
	SDL_Surface *screen;
	SDL_Surface *squares;
	SDL_Surface *buttons;

	// The font that's going to be used
	TTF_Font* font;
	TTF_Font* menuFonts[3];

	// The portions of the sprite map to be blitted
	SDL_Rect boardClip[5];
	SDL_Rect clip[12];
	SDL_Rect sqClip[4];
	SDL_Rect btClip[9];

	bool fullscreen;
	bool quit;

	NetGame* netGame;

	GBoard* board;
	GLog* log;
	Player* player;
	Player* opponent;
	Button* menuButton;
	NameLabel* downName;
	NameLabel* upName;
	Indicator* curPlayer;
	// Container for the menu items
	Menu* menu;
	MenuItem* newMenu;
	MenuItem* endMenu;
	MessageBox* message;

	// Routines for sprite sheet clips definition
	void defineBoardClips();
	void definePieceClips();
	void defineSquareClips();
	void defineButtonClips();

	void setupBoard();
	void setupButtons();
	void setupLabels();
	void setupMenu();
	void setupMainMenu();
	void setupNameMenu();
	void setupDrawMenu();
	void setupPromoteMenu();

public:
	Gui();

	~Gui();

	SDL_Surface* getScreen();

	GBoard* getGBoard();

	Indicator* getCurPlayer();

	Menu* getMenu();

	bool canQuit();

	/**
	 * Initialize all SDL subsystems. Also initializes SDL TTF
	 * and sets up the screen.
	 * @return true if and only if SDL was successfully initialized;
	 * false otherwise
	 */
	bool init();

	/**
	 * Frees the surfaces, closes the font files, and quits SDL.
	 */
	void cleanUp();

	/**
	 * Loads all required sprite sheets and fonts.
	 * @return true if and only if all files were loaded successfully;
	 * false otherwise
	 */
	bool loadFiles();

	/**
	 * Defines sprite sheet clips.
	 */
	void defineClips();

	/**
	 * Toggles between fullscreen and windowed mode
	 */
	void toggleFullscreen();

	/**
	 * Callback to non-static member `toggleFullscreen()',
	 * implemented as a static wrapper.
	 * @param object the this-pointer of a class object
	 */
	static void cmdToggleFullscreen(void* object);

	static void cmdChLang(void* object, void* arg);

	static void cmdToggleMenu(void* object);

	static void cmdExit(void* object);

	static void cmdHost(void* object);

	static void cmdJoin(void* object, void* arg);

	static void cmdDraw(void* object);

	static void cmdAcceptDraw(void* object);

	static void cmdDeclineDraw(void* object);

	static void cmdResign(void* object);

	static void cmdSetPlayer(void* object, void* arg);

	static void cmdPromote(void* object, void* arg);

	static void cmdFlipBoard(void* object);

	void toggleMenu();

	/**
	 * Handles a generic event
	 * @param the event to be handled
	 */
	void handleEvent(SDL_Event event);

	/**
	 * Sets up the board. the buttons, the labels and others.
	 */
	void setupGameComponents();

	void cleanUpGameComponents();

	void showGameComponents();
};

