#include "lang.h"
#include <cstring>
#include <iostream>
#include <fstream>

Lang* Lang::instance = NULL;

Lang::Lang() {
	load(LANG_EN);
}

Lang* Lang::getInstance() {
	if(instance == NULL) {
		instance = new Lang;
	}
	return instance;
}

int Lang::load(int language) {
	const char* fileName;
	switch(language) {
	case LANG_EN:
		fileName = "en.lang";
		break;
	case LANG_RO:
		fileName = "ro.lang";
		break;
	default:
		return -1;
	}

	strings.clear();

	std::string line;
	std::ifstream langFile(fileName);
	if(langFile.is_open()) {
		while(! langFile.eof()) {
			std::getline(langFile, line);
			insert(line);
		}
		langFile.close();
	} else {
		std::cerr << "Unable to open file" << std::endl;
		return -1;
	}
	return 0;
}

std::string Lang::str(std::string key) {
	std::map<std::string, std::string>::iterator i;
	for(i = strings.begin(); i != strings.end(); i++) {
		if((*i).first == key) {
			return (*i).second;
		}
	}
	// default:
	return key;
}

void Lang::insert(std::string line) {
	char* key;
	char* val;
	key = strtok((char*)line.c_str(), "=");
	if(key != NULL) {
		val = strtok(NULL, "=");
		if(val != NULL) {
			strings[key] = val;
		}
	}
}

std::string i18n(std::string text) {
	return Lang::getInstance()->str(text);
}

