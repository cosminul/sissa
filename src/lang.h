#ifndef LANG_H
#define LANG_H

#include <string>
#include <map>

class Lang {
private:
	static Lang* instance;
	std::map<std::string, std::string> strings;

	void insert(std::string line);

protected:
	Lang();

public:
	static Lang* getInstance();

	static const int LANG_EN = 1;
	static const int LANG_RO = 2;

	int load(int language);

	std::string str(std::string key);
};

std::string i18n(std::string);

#endif

