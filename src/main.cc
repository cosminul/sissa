#include <SDL/SDL.h>
#include "gui.h"

// The event structure that will be used
SDL_Event event;

int main(int argc, char* args[]) {
	Gui gameGui;

	// Initialize
	if(gameGui.init() == false) {
		return 1;
	}

	// Load the files
	if(gameGui.loadFiles() == false) {
		return 1;
	}

	gameGui.defineClips();
	gameGui.setupGameComponents();

	// While the user hasn't quit
	while(gameGui.canQuit() == false) {
		// While there's an event to handle
		while(SDL_PollEvent(&event)) {
			gameGui.handleEvent(event);
		}

		gameGui.showGameComponents();

		// Update the screen
		if(SDL_Flip(gameGui.getScreen()) == -1) {
			return 1;
		}

		SDL_Delay(100);
	}

	// Free the surfaces and quit SDL
	gameGui.cleanUp();

	return 0;
}

