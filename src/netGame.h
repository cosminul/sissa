#ifndef NET_GAME_H
#define NET_GAME_H

#include <SDL/SDL_net.h>
#include "observer.h"
#include "sissa.h"

class Gui;

class NetGame : public Observer {
private:
	/** Socket descriptor */
	TCPsocket sd;

	/** Server socket descriptor */
	TCPsocket ssd;

	SDLNet_SocketSet set;

	IPaddress ip;
	IPaddress *remoteIP;

	char inBuffer[512];
	char outBuffer[512];

	bool ready; // ready
	bool connected; // ready and have a peer

	bool outBufferLock;

	/** The host we are connecting to */
	char* remoteHost;

	/**
	 * The board we are playing on. This is also the observable subject.
	 */
	Board* board;
	Gui* gui;

	Player* player;
	Player* opponent;

	/** Send and receive messages */
	int communicate();

	/** Host a game */
	int hostGame();

	/** Wrapper for the `hostGame()' method. Needed for SDL threads */
	static int hostGame(void* t);

	/** Join a game */
	int joinGame();

	/** Wrapper for the `joinGame()' method. Needed for SDL threads */
	static int joinGame(void* t);

	void interpret(const char* message);

public:
	static const int CANNOT_RESOLVE_HOST = 1;
	static const int CANNOT_OPEN_SOCKET = 2;

	NetGame(Gui* gui, Player* player);

	~NetGame();

	void host();

	void join(char* host);

	void disconnect();

	void send(const char* message);

	bool isReady();

	bool isConnected();

	void update(Observable* theChangedSubject);

	Player* getPlayer();

	Player* getOpponent();
};

#endif

