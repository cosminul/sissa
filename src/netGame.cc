#include "netGame.h"
#include "util.h"
#include "gui.h"
#include <string>
#include <iostream>

NetGame::NetGame(Gui* gui, Player* player) {
	this->gui = gui;
	this->board = gui->getGBoard()->getBoard();
	this->player = player;
	this->opponent = new Player("?");
	ready = false;
	connected = false;
	set = SDLNet_AllocSocketSet(1);
	outBufferLock = false;
	strcpy(outBuffer, "");
	board->attach(this);
}

NetGame::~NetGame() {
	disconnect();
	board->detach(this);
}

void NetGame::host() {
	remoteHost = NULL;
	SDL_CreateThread(hostGame, this);
}

void NetGame::join(char* host) {
	remoteHost = host;
	SDL_CreateThread(joinGame, this);
}

void NetGame::disconnect() {
	ready = connected = false;
	SDLNet_TCP_Close(sd);
	SDLNet_TCP_Close(ssd);
	SDLNet_TCP_DelSocket(set, sd);
/*
	board->setCurrent(NULL);
	board->notify();
*/	
	gui->getCurPlayer()->setVisible(false);
	opponent->setName("?");
	opponent->standUp();
}

void NetGame::send(const char* message) {
	while(outBufferLock);
	outBufferLock = true;

	strcpy(outBuffer, message);

	outBufferLock = false;
}

int NetGame::communicate() {
	if(SDLNet_TCP_AddSocket(set, sd) == -1) {
		std::cerr << "SDLNet_TCP_AddSocket: ";
		std::cerr << SDLNet_GetError() << std::endl;
	}
	connected = true;
	while(connected) {
		SDL_Delay(100);
		int numReady = SDLNet_CheckSockets(set, 0);
		if(numReady == -1) {
			std::cerr << "SDLNet_CheckSockets: ";
			std::cerr << SDLNet_GetError() << std::endl;
			break;
		}
		// Check to see if the peer sent us data
		if(numReady && SDLNet_SocketReady(sd)) {
			if (SDLNet_TCP_Recv(sd, inBuffer, 512) > 0) {
				interpret(inBuffer);
			}
		}

		while(outBufferLock);
		outBufferLock = true;
		int len = strlen(outBuffer) + 1;
		// Is there something in the output buffer?
		if(len < 2) {
			// No message
		} else if (SDLNet_TCP_Send(sd, (void *)outBuffer, len) < len) {
			std::cerr << "SDLNet_TCP_Send: ";
			std::cerr << SDLNet_GetError() << std::endl;
			connected = false;
		}
		if(outBuffer[1] == '!') {
			disconnect();
		}
		strcpy(outBuffer, ""); // empty the buffer
		outBufferLock = false;
	}
	return 0;
}

int NetGame::hostGame() {
	// Resolve the host; NULL makes the network interface to listen
	if(SDLNet_ResolveHost(&ip, NULL, 9876) < 0) {
		std::cerr << "Cannot host game: " << std::endl;
		std::cerr << SDLNet_GetError() << std::endl;
		return CANNOT_RESOLVE_HOST;
	}
	// Open a connection with the IP provided (listen on the host's port)
	if(!(ssd = SDLNet_TCP_Open(&ip))) {
		std::cerr << "Cannot open socket: ";
		std::cerr << SDLNet_GetError() << std::endl;
		return CANNOT_OPEN_SOCKET;
	}

	board->init();
	player->sit(board, Player::WHITE);
	opponent->sit(board, Player::BLACK);
	board->setCurrent(player);

	/* Check the ssd if there is a pending connection. If there is one,
	 * accept that, and open a new socket for communicating */
	ready = true;
	connected = false;
	while(!connected) {
		SDL_Delay(200);
		if((sd = SDLNet_TCP_Accept(ssd))) {
			connected = true;
			/* Now we can communicate with the client using
			 * sd socket. ssd will remain opened waiting other
			 * connections */

			/* Get the remote address */
			if((remoteIP = SDLNet_TCP_GetPeerAddress(sd))) {
				/* Print the address, converting
				 * in the host format */
				printf("Host connected: %x %d\n", SDLNet_Read32(&remoteIP->host), SDLNet_Read16(&remoteIP->port));
			} else {
				fprintf(stderr, "SDLNet_TCP_GetPeerAddress: %s\n", SDLNet_GetError());
			}
		}
	}

	communicate();
	return 0;
}

int NetGame::hostGame(void* t) {
	return ((NetGame*)t)->hostGame();
}

int NetGame::joinGame() {
	// Resolve the host we are connecting to
	if(SDLNet_ResolveHost(&ip, remoteHost, 9876) < 0) {
		std::cerr << "Cannot join game: " << std::endl;
		std::cerr << SDLNet_GetError() << std::endl;
	}
	// Open a connection with the IP provided (listen on the host's port)
	if(!(sd = SDLNet_TCP_Open(&ip))) {
		std::cerr << "Cannot open socket: ";
		std::cerr << SDLNet_GetError() << std::endl;
	}

	board->init();
	player->sit(board, Player::BLACK);
	opponent->sit(board, Player::WHITE);
	board->setCurrent(opponent);

	// Introduce ourselves to the host
	char msg[256];
	strcpy(msg, "h");
	strcat(msg, player->getName().c_str());
	send(msg);

	communicate();
	return 0;
}

int NetGame::joinGame(void* t) {
	return ((NetGame*)t)->joinGame();
}

void NetGame::interpret(const char* message) {
	std::string msg(message);

	switch(message[0]) {
	case 'd': // offer [d]raw
		std::cout << "peer offers draw" << std::endl;
		gui->getMenu()->setRootMenuItem("$draw_root");
		gui->toggleMenu();
		break;
	case 'h': // [h]ello (the opponent says his name)
		std::cout << "setting name of " << opponent << "=";
		std::cout << msg.substr(1) << std::endl;
		opponent->setName(msg.substr(1));
		// answer with our own name
		char name[256];
		strcpy(name, "j");
		strcat(name, player->getName().c_str());
		send(name);
		break;
	case 'j': // the opponent answer with their name ([j] was next to [h])
		std::cout << "setting name of " << opponent << "=";
		std::cout << msg.substr(1) << std::endl;
		opponent->setName(msg.substr(1));
		break;
	case 'n': // peer said [n]o to draw offer
		board->setMessage(Board::MSG_NO_DRAW);
		board->notify();
		board->setMessage(Board::MSG_NONE);
		break;
	case 'y': // peer said [y]es to draw offer
		board->setMessage(Board::MSG_YES_DRAW);
		disconnect();
		board->setMessage(Board::MSG_RESIGN);
		break;
	case 'p': // [p]romoted
	{
		// brackets are necessary when declaring variables
		// within a case label
		std::string pos = msg.substr(1, 2);
		Piece* pawn = board->pickUp(pos[0], pos[1] - '0');
		int identity = atoi(msg.substr(4).c_str());
		((Pawn*)pawn)->promote(identity);
	}
		break;
	case 'r': // peer [r]esigns
		std::cout << "peer resigns" << std::endl;
		board->setMessage(Board::MSG_RESIGN);
		disconnect();
		board->setMessage(Board::MSG_NONE);
		break;
	case 'm': // [m]ove; this is also the default option
	default:
		std::string from = msg.substr(1, 2);
		std::string to = msg.substr(3, 2);
		board->pickUp(from[0], from[1] - '0')->move(to[0], to[1] - '0');
	}
}

bool NetGame::isReady() {
	return ready;
}

bool NetGame::isConnected() {
	return connected;
}

void NetGame::update(Observable* theChangedSubject) {
	if(theChangedSubject != board) {
		return;
	}
	if(board->getCurrent() != opponent) {
		return;
	}
	if(board->getLogger()->getLastMove() == NULL) {
		return;
	}
	std::string from = board->getLogger()->getLastMove()->getStart();
	std::string to = board->getLogger()->getLastMove()->getEnd();

	char msg[16];
	switch(board->getMessage()) {
	case Board::MSG_PROMOTED:
		// promoted
		{
		int identity = *((int*)board->getMessageData());
		strcpy(msg, "p");
		strcat(msg, to.c_str());
		strcat(msg, ":");
		strcat(msg, intToStr(identity).c_str());
		}
		break;
	default:
		// piece move
		strcpy(msg, "m");
		strcat(msg, from.c_str());
		strcat(msg, to.c_str());
		break;
	}
	send(msg);
}

Player* NetGame::getPlayer() {
	return player;
}

Player* NetGame::getOpponent() {
	return opponent;
}

