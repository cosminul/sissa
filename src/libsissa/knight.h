#ifndef KNIGHT_H
#define KNIGHT_H

#include "piece.h"

/**
 * Knight is a Piece.
 *
 * @date 2006 12 18
 */
class Knight : public Piece {
public:
	static const int WHITE = 10;
	static const int BLACK = 11;

	Knight(int color);

	~Knight();

	std::vector<Square*> unsafeValidMoves();

	int getAppearance();
};

#endif

