#ifndef OBSERVER_H
#define OBSERVER_H

class Observable;

class Observer {
public:
	virtual ~Observer();
	virtual void update(Observable* theChangedSubject) = 0;

protected:
	Observer();
};

#endif

