#ifndef LOGGER_H
#define LOGGER_H

#include <vector>
#include <string>
#include "square.h"

class Logger {
public:
	class Move {
	private:
		std::string start;
		std::string end;
		std::string piece;
		int special;

	public:
		static const int KINGSIDE_CASTLING = 1;
		static const int QUEENSIDE_CASTLING = 2;

		Move(Square* start, Square* end);

		Move(Square* start, Square* end, int specialMove);

		std::string getStart();

		std::string getEnd();

		std::string getPiece();

		int getSpecial();
	};

protected:
	std::vector<Move*> moves;

public:
	Logger();

	virtual ~Logger();

	Logger::Move* getLastMove();

	void log(Move* move);

	virtual std::string toString() = 0;

	void clear();
};

#endif

