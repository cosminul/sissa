#include "caretaker.h"

Caretaker* Caretaker::instance = NULL;

Caretaker::Caretaker() {
	savedStates.reserve(20);
}

Caretaker* Caretaker::getInstance() {
	if(instance == NULL) {
		instance = new Caretaker;
	}
	return instance;
}

int Caretaker::addMemento(Memento* memento) {
	savedStates.push_back(memento);
	return savedStates.size() - 1;
}

Memento* Caretaker::getMemento(int index) {
	return savedStates[index];
}

void Caretaker::removeMemento(int index) {
	std::vector<Memento*>::iterator i = savedStates.begin();
	savedStates.erase(i + index);
}

