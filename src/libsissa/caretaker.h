#ifndef CARETAKER_H
#define CARETAKER_H

#include <vector>
#include "memento.h"

/**
 * Caretaker - used in the Memento pattern.
 * @author Cosmin
 * @date 2007 03 29
 */
class Caretaker {
private:
	std::vector<Memento*> savedStates;

	static Caretaker* instance;

protected:
	Caretaker();

public:
	static Caretaker* getInstance();

	/**
	 * @return the position where the memento was added
	 */
	int addMemento(Memento* memento);

	Memento* getMemento(int index);

	void removeMemento(int index);
};

#endif

