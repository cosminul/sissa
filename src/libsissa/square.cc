#include <sstream>
#include "square.h"

#include "piece.h"

const int Square::WHITE;
const int Square::BLACK;

Square::Square() {
	// ...
}

Square::Square(char file, int rank) {
	this->file = file;
	this->rank = rank;
	// Start with a black square, at `a1'.
	color = ((file - 'a') + (rank - 1)) % 2 ? WHITE : BLACK;
	piece = NULL;
}

Square::~Square() {
	// bye bye, square
}

char Square::getFile() {
	return file;
}

int Square::getRank() {
	return rank;
}

int Square::getColor() {
	return color;
}

void Square::setColor(int color) {
	this->color = color;
}

Piece* Square::getPiece() {
	return piece;
}

void Square::setPiece(Piece* piece) {
	this->piece = piece;
}

bool Square::isFree() {
	return piece == NULL;
}

bool Square::isFriendOf(Square* s) {
	if(piece == NULL || s->piece == NULL) {
		return false;
	}
	return piece->getColor() == s->piece->getColor();
}

bool Square::isOpponentOf(int color) {
	return (piece != NULL) && (piece->getColor() != color);
}

std::string Square::toString() {
	// Convert the int to string using stringstream
	std::stringstream ss;
	std::string strRank;
	ss << rank;
	ss >> strRank;
	// Return the square name (eg, d7)
	return file + strRank;
}

