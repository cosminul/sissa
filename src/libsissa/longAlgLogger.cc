#include "longAlgLogger.h"
#include "piece.h"
#include "util.h" // for intToStr(int)

LongAlgebraicLogger::LongAlgebraicLogger() : Logger() {
}

LongAlgebraicLogger::~LongAlgebraicLogger() {
}

std::string LongAlgebraicLogger::toString() {
	int index = 3;
	std::string str = "";
	std::vector<Move*>::iterator i;
	for(i = moves.begin(); i != moves.end(); i++) {
		if (index % 2 == 1) {
			str.append(intToStr(index / 2));
			str.append(". ");
		}
		int special = (*i)->getSpecial();
		if(special == Logger::Move::KINGSIDE_CASTLING) {
			str.append("0-0");
		} else if(special == Logger::Move::QUEENSIDE_CASTLING) {
			str.append("0-0-0");
		} else {
			str.append((*i)->getPiece());
			str.append((*i)->getStart());
			str.append("-");
			str.append((*i)->getEnd());
		}
		if(index % 2 == 1) {
			str.append(" ");
		} else {
			str.append("\n");
		}
		index++;
	}
	return str;
}

