#ifndef PIECE_H
#define PIECE_H

#include <string>
#include <vector>
#include "caretaker.h"
#include "player.h"

// Using forward declaration instead of include directive
class Board;
class Square;

/**
 * This class represents the parent class for all chess pieces,
 * and contains ancestors for all the common properties of chess pieces.
 *
 * @date 2006 11 03
 */
class Piece {
protected:
	static const int DIRECTION_N = 1;
	static const int DIRECTION_E = 2;
	static const int DIRECTION_S = 4;
	static const int DIRECTION_W = 8;
	static const int DIRECTION_NE =  3; // N | E
	static const int DIRECTION_SE =  6; // S | E
	static const int DIRECTION_SW = 12; // S | W
	static const int DIRECTION_NW =  9; // N | W

	/**
	 * The color of the piece: either white (light) or black (dark)
	 */
	int color;

	/**
	 * A pointer to the board the piece sits on. It needs to see the
	 * board in order to make move decissions
	 */
	Board* board;

	/**
	 * A pointer to the square the piece sits on. The current position,
	 * that is.
	 */
	Square* square;

	/**
	 * A vector of possibly valid moves.
	 */
	std::vector<Square*> valid;

	std::string name;

	/**
	 * The letter that describes the piece in algebraic notation.
	 * It is a string, instead of a char, for i18n reasons: Some
	 * languages might use two letters for some pieces. Also, the pawn
	 * needs no letter, so it will be assigned a zero-length string.
	 */
	std::string letter;

	bool moved;

	void changeBoardMessage();

	bool canMoveTo(char file, int rank);

	bool addToValid(char file, int rank);

	bool addToValid(Square* square);

	void addToValid(int direction);

	void removeFromValid(char file, int rank);

	bool isSafeToMoveAt(char file, int rank);

	virtual void ignoreUnsafeMoves();

	/**
	 * Ask the board to remember its layout before moving this piece
	 * to (file, rank)
	 */
	virtual void takeBoardSnapshot(char file, int rank);

public:
	/** The white (light) color */
	static const int WHITE = 0;
	/** The black (dark) color */
	static const int BLACK = 1;

	// It is advised that if a class contains virtual methods,
	// the destructor should also be virtual.
	virtual ~Piece() {};

	/**
	 * @return the name of the piece
	 */
	std::string getName();

	/**
	 * @return the piece abbreviation
	 */
	std::string getLetter();

	/**
	 * Returns the color of the piece. It should be either WHITE (0)
	 * or BLACK (1).
	 *
	 * @return the color of the piece
	 */
	int getColor();

	/**
	 * Sets the color of the piece. It should be either WHITE (0)
	 * or BLACK (1).
	 *
	 * @param color the new color of the piece
	 */
	void setColor(int color);

	/**
	 * @return the Board this Piece sits on
	 */
	Board* getBoard();

	/**
	 * @param board a pointer to a Board
	 */
	void setBoard(Board* board);

	/**
	 * @return the Square this Piece sits on
	 */
	Square* getSquare();

	/**
	 * Moves this Piece to a new Square
	 * @param square a pointer to a Square
	 */
	void setSquare(Square* square);

	/**
	 * @return a pointer to the player who owns this piece
	 */
	Player* getOwner();

	virtual void simulateMove(char file, int rank);

	virtual bool move(char file, int rank);

	bool wasMoved();

	/**
	 * Builds the valid moves array, without minding safety
	 * @return a vector of safe and unsafe moves
	 */
	virtual std::vector<Square*> unsafeValidMoves() = 0;

	virtual std::vector<Square*> validMoves();

	virtual int getAppearance() = 0;
};

#endif

