#include "bishop.h"
#include "square.h"
#include "board.h"

Bishop::Bishop(int color) {
	name = "bishop";
	letter = "B";
	this->color = color;
	moved = false;
	valid.reserve(14);
}

Bishop::~Bishop() {
	// bye bye, bishop
}

/**
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> Bishop::unsafeValidMoves() {
	valid.clear();
	addToValid(Piece::DIRECTION_NE);
	addToValid(Piece::DIRECTION_SE);
	addToValid(Piece::DIRECTION_SW);
	addToValid(Piece::DIRECTION_NW);
	return valid;
}

int Bishop::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

