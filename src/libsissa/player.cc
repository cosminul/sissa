#include "player.h"
#include "board.h"
#include "square.h"
#include "king.h"
#include <typeinfo>
#include <cstring>

Player::Player() {
	color = Player::WHITE;
	board = NULL;
	pieces.reserve(16);
	moves.reserve(32);
}

Player::Player(std::string name) {
	color = Player::WHITE;
	board = NULL;
	pieces.reserve(16);
	moves.reserve(32);
	this->name = name;
}

Player::~Player() {
	// TODO
}

int Player::getColor() {
	return color;
}

void Player::setColor(int color) {
	this->color = color;
}

std::string Player::getName() {
	return name;
}

void Player::setName(std::string name) {
	this->name = name;
	if(board != NULL) {
		board->notify();
	}
}

bool Player::sit(Board* board, int color) {
	if(color == Player::WHITE) {
		if(board->getWhite() != NULL) {
			return false;
		}
		board->setWhite(this);
	} else if(color == Player::BLACK) {
		if(board->getBlack() != NULL) {
			return false;
		}
		board->setBlack(this);
	}
	this->board = board;
	this->color = color;	
	return true;
}

bool Player::standUp() {
	if(! isSeated()) {
		return false;
	}
	if(color == Player::WHITE) {
		board->setWhite(NULL);
	} else if(color == Player::BLACK) {
		board->setBlack(NULL);
	}
	this->board = NULL;
//!	board->setMessage(Board::MSG_LEAVE);
//!	board->notify();
	return true;
}

bool Player::sitOrStandUp(Board* board, int color) {
	if(isSeated()) {
		return standUp();
	} else {
		return sit(board, color);
	}
}

bool Player::isSeated() {
	return board != NULL;
}

bool Player::isCurrentPlayer() {
	return (board->getCurrent() == this);
}

Player* Player::getOpponent() {
	if(board->getWhite() == this) {
		return board->getBlack();
	} else {
		return board->getWhite();
	}
}

Board* Player::getBoard() {
	return board;
}

void Player::addPiece(Piece* piece) {
	if(piece == NULL) {
		return;
	}
	if(piece->getOwner() != this) {
		return;
	}
	// Do not add if already in collection
	std::vector<Piece*>::iterator i;
	for(i = pieces.begin(); i != pieces.end(); i++) {
		if(*i == piece) {
			return;
		}
	}
	pieces.push_back(piece);
}

void Player::removePiece(Piece* piece) {
	std::vector<Piece*>::iterator i = pieces.begin();
	while(i != pieces.end()) {
		if(*i == piece) {
			i = pieces.erase(i);
		} else {
			i++;
		}
	}
}

void Player::readPieces() {
	for(char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			Square* s = board->getSquare(file + 'a', rank + 1);
			addPiece(s->getPiece());
		}
	}
}

std::vector<Piece*> Player::getPieces() {
	if(pieces.size() == 0) {
		readPieces();
	}
	return pieces;
}

std::vector<Square*> Player::getPossibleMoves() {
	moves.clear();
	// Make sure the pieces vector is up-to-date
	if(pieces.size() == 0) {
		readPieces();
	}
	std::vector<Piece*>::iterator i;
       	for(i = pieces.begin(); i != pieces.end(); i++) {
		addToPossibleMoves((*i)->unsafeValidMoves());
	}
	return moves;
}

void Player::addToPossibleMoves(std::vector<Square*> squares) {
	std::vector<Square*>::iterator i;
       	for(i = squares.begin(); i != squares.end(); i++) {
		// If the current square is already in the valid moves
		// vector, skip it
		if(!canMoveTo(*i)) {
			moves.push_back(*i);
		}
	}
}

bool Player::canMoveTo(Square* square) {
	// Iterate the possible moves vector and search for `square'
	std::vector<Square*>::iterator i = moves.begin();
	while(i != moves.end()) {
		if(*i == square) {
			return true;
		} else {
			i++;
		}
	}
	return false;
}

bool Player::kingIsInCheck() {
	King* king;
	for(char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			Square* s = board->getSquare(file + 'a', rank + 1);
			Piece* p = s->getPiece();
			if(p == NULL || p->getOwner() != this) {
				continue;
			}
			if(strstr(typeid(*p).name(), "King") != NULL) {
				king = (King*)s->getPiece();
			}
		}
	}
	getOpponent()->getPossibleMoves();
	return getOpponent()->canMoveTo(king->getSquare());
}

bool Player::kingIsCheckmated() {
	// Make sure the pieces vector is up-to-date
	if(pieces.size() == 0) {
		readPieces();
	}
	std::vector<Piece*>::iterator i;
       	for(i = pieces.begin(); i != pieces.end(); i++) {
		std::vector<Square*> m = (*i)->validMoves();
		if(m.size() > 0) {
			return false;
		}
	}
	return true;
}

