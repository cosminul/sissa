#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include "observer.h"
#include <vector>

/**
 * An observable object is a subject in the Observer design pattern
 * @date 2007 04 15
 */
class Observable {
public:
	virtual ~Observable();

	virtual void attach(Observer* observer);
	virtual void detach(Observer* observer);
	virtual void notify();

protected:
	Observable();

private:
	std::vector<Observer*> observers;
};

#endif

