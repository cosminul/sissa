#include "piece.h"
#include "square.h"
#include "board.h"
#include "logger.h"

std::string Piece::getName() {
	return name;
}

std::string Piece::getLetter() {
	return letter;
}

int Piece::getColor() {
	return color;
}

void Piece::setColor(int color) {
	this->color = color;
}

Board* Piece::getBoard() {
	return board;
}

void Piece::setBoard(Board* board) {
	this->board = board;
}

Square* Piece::getSquare() {
	return square;
}

void Piece::setSquare(Square* square) {
	this->square = square;
}

Player* Piece::getOwner() {
	return (color == WHITE) ? board->getWhite() : board->getBlack();
}

bool Piece::canMoveTo(char file, int rank) {
	validMoves();

	std::vector<Square*>::iterator i = valid.begin();
	while(i != valid.end()) {
		if(*i == board->getSquare(file, rank)) {
			return true;
		} else {
			i++;
		}
	}
	return false;
}

bool Piece::addToValid(char file, int rank) {
	// Do not allow out-of-bounds coordinates
	if(file < 'a' || file > 'h') {
		return false;
	}
	if(rank < 1 || rank > 8) {
		return false;
	}
	// Do not allow self
	if(file == square->getFile() && rank == square->getRank()) {
		return false;
	}
	// Do not allow a squared occupied by a piece of the same color
	Square* sq = board->getSquare(file, rank);
	if(!sq->isFree() && !sq->isOpponentOf(color)) {
		return false;
	}
	// If everything went right, push in vector
	valid.push_back(board->getSquare(file, rank));
	return true;
}

bool Piece::addToValid(Square* square) {
	if(square == NULL) {
		return false;
	}
	return addToValid(square->getFile(), square->getRank());
}

void Piece::addToValid(int direction) {
	char f = square->getFile();
	int r = square->getRank();
	Square* s;
	switch(direction) {
	case DIRECTION_N:
		do {
			s = board->getSquare(f, ++r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_E:
		do {
			s = board->getSquare(++f, r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_S:
		do {
			s = board->getSquare(f, --r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_W:
		do {
			s = board->getSquare(--f, r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_NE:
		do {
			s = board->getSquare(++f, ++r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_SE:
		do {
			s = board->getSquare(++f, --r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_SW:
		do {
			s = board->getSquare(--f, --r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	case DIRECTION_NW:
		do {
			s = board->getSquare(--f, ++r);
			addToValid(s);
		} while(s != NULL && s->isFree());
		break;
	}	
}

void Piece::removeFromValid(char file, int rank) {
	std::vector<Square*>::iterator i = valid.begin();
	while(i != valid.end()) {
		if((*i)->getFile() == file && (*i)->getRank() == rank) {
			i = valid.erase(i);
		} else {
			i++;
		}
	}
}

bool Piece::wasMoved() {
	return moved;
}

std::vector<Square*> Piece::validMoves() {
	unsafeValidMoves();
	ignoreUnsafeMoves();
	return valid;
}

bool Piece::move(char file, int rank) {
	Square* oldSquare = getSquare();
	if(canMoveTo(file, rank) == false) {
		return false;
	}
	simulateMove(file, rank);

	board->getLogger()->log(new Logger::Move(oldSquare, getSquare()));
	changeBoardMessage(); // Say 'checkmate!', or 'check!' if needed
	moved = true;
	board->endTurn();

	return true;
}

void Piece::simulateMove(char file, int rank) {
	// Leave the current location
	Square* s = square;
	square->getPiece()->setSquare(NULL);
	s->setPiece(NULL);

	// Move to the new location
	square = board->getSquare(file, rank);
	Piece* p = square->getPiece();
	if(p != NULL) {
		p->getOwner()->removePiece(p);
	}
	square->setPiece(this);
	setSquare(square);
}

bool Piece::isSafeToMoveAt(char file, int rank) {
	bool safe;
	// Set originator's state
	takeBoardSnapshot(file, rank);
	Caretaker::getInstance()->addMemento(board->saveToMemento());
	// Do move
	simulateMove(file, rank);
	// Check safety conditions
	safe = !getOwner()->kingIsInCheck();
	// Undo move
	board->restoreFromMemento(Caretaker::getInstance()->getMemento(0));
	Caretaker::getInstance()->removeMemento(0);

	return safe;
}

void Piece::ignoreUnsafeMoves() {
	// Iterate through the `valid' vector and remove any unsafe move
	std::vector<Square*>::iterator i = valid.begin();
	while(i != valid.end()) {
		if(!isSafeToMoveAt((*i)->getFile(), (*i)->getRank())) {
			i = valid.erase(i);
		} else {
			i++;
		}
	}
}

void Piece::takeBoardSnapshot(char file, int rank) {
	board->saveBeforeMove(square, board->getSquare(file, rank));
}

void Piece::changeBoardMessage() {
	board->clearMessage();
	if(getOwner()->getOpponent()->kingIsInCheck()) {
		if(getOwner()->getOpponent()->kingIsCheckmated()) {
			board->setMessage(Board::MSG_CHECKMATE);
		} else {
			board->setMessage(Board::MSG_CHECK);
		}
	}
}

