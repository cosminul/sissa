#ifndef KING_H
#define KING_H

#include "piece.h"

/**
 * King is a Piece.
 *
 * @date 2006 12 18
 */
class King : public Piece {
private:
	bool canCastleOnTheKingside();
	bool canCastleOnTheQueenside();

	void ignoreUnsafeMoves();

public:
	static const int WHITE = 0;
	static const int BLACK = 1;

	King(int color);

	~King();

	std::vector<Square*> unsafeValidMoves();

	bool move(char file, int rank);

	int getAppearance();
};

#endif

