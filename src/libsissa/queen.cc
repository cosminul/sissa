#include "queen.h"
#include "square.h"
#include "board.h"

Queen::Queen(int color) {
	name = "queen";
	letter = "Q";
	this->color = color;
	moved = false;
	valid.reserve(28);
}

Queen::~Queen() {
	// bye bye, queen
}

/**
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> Queen::unsafeValidMoves() {
	valid.clear();
	addToValid(Piece::DIRECTION_N);
	addToValid(Piece::DIRECTION_E);
	addToValid(Piece::DIRECTION_S);
	addToValid(Piece::DIRECTION_W);
	addToValid(Piece::DIRECTION_NE);
	addToValid(Piece::DIRECTION_SE);
	addToValid(Piece::DIRECTION_SW);
	addToValid(Piece::DIRECTION_NW);
	return valid;
}

int Queen::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

