#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include "piece.h"
#include "square.h"

class Board;

/**
 * A Player can participate in a chess game.
 *
 * @date 2006 11 14
 */
class Player {
private:
	int color;

	std::string name;

	std::vector<Piece*> pieces;

	/** All the possible moves */
	std::vector<Square*> moves;

	Board* board;

	/**
	 * Force reading pieces. Usually called when the `pieces' vector
	 * is needed, but was not initialized.
	 */
	void readPieces();

	void addToPossibleMoves(std::vector<Square*> squares);

public:
	/** The white (light) color */
	static const int WHITE = 0;
	/** The black (dark) color */
	static const int BLACK = 1;

	/** Creates a new Player */
	Player();

	Player(std::string name);

	~Player();

	int getColor();

	void setColor(int color);

	std::string getName();

	void setName(std::string name);

	bool sit(Board* board, int color);

	bool standUp();

	bool sitOrStandUp(Board* board, int color);

	bool isSeated();

	bool isCurrentPlayer();

	Player* getOpponent();

	Board* getBoard();

	/**
	 * @param piece the Piece to be added
	 */
	void addPiece(Piece* piece);

	void removePiece(Piece* piece);

	std::vector<Piece*> getPieces();

	std::vector<Square*> getPossibleMoves();

	bool canMoveTo(Square* square);

	bool kingIsInCheck();

	bool kingIsCheckmated();
};

#endif

