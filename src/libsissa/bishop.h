#ifndef BISHOP_H
#define BISHOP_H

#include "piece.h"

/**
 * Bishop is a Piece.
 *
 * @date 2006 12 18
 */
class Bishop : public Piece {
public:
	static const int WHITE = 6;
	static const int BLACK = 7;

	Bishop(int color);

	~Bishop();

	std::vector<Square*> unsafeValidMoves();

	int getAppearance();
};

#endif

