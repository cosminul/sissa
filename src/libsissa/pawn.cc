#include "pawn.h"
#include "square.h"
#include "board.h"
#include "logger.h"
// Include the definitions of the pieces needed for promotion
#include "rook.h"
#include "knight.h"
#include "bishop.h"
#include "queen.h"
#include <typeinfo>
#include <iostream>
#include <cstdlib>
#include <cstring>

Pawn::Pawn(int color) {
	name = "pawn";
	letter = "";
	this->color = color;
	if(color == Piece::WHITE) {
		advance = 1;
		homeRank = 2;
	} else {
		advance = -1;
		homeRank = 7;
	}
	passing = false;
	moved = false;
	// Reserve 2 in front and 2 for capture
	valid.reserve(4);
}

Pawn::~Pawn() {
	// bye bye, pawn
}

/**
 * A white pawn can increase its rank. A black one can decrease it.
 * The file can only change by 1 when attacking.
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> Pawn::unsafeValidMoves() {
	valid.clear();
	char file = square->getFile();
	int rank = square->getRank();
	// Advance one square
	Square* next = board->getSquare(file, rank + advance);
	if(next != NULL && next->isFree()) {
		addToValid(next);
		// Advance two squares
		next = board->getSquare(file, rank + 2 * advance);
		if(square->getRank() == homeRank && next->isFree()) {
			addToValid(next);
		}
	}
	// Capture left
	next = board->getSquare(file - 1, rank + advance);
	if(next != NULL && (!next->isFree() || canCaptureEnPassant(file - 1))) {
		addToValid(next);
	}
	// Capture right
	next = board->getSquare(file + 1, rank + advance);
	if(next != NULL && (!next->isFree() || canCaptureEnPassant(file + 1))) {
		addToValid(next);
	}
	return valid;
}

int Pawn::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

bool Pawn::canCaptureEnPassant(char file) {
	Pawn* pawn;
	Square* next = board->getSquare(file, square->getRank());
	if(next != NULL && !next->isFree()) {
		Piece* p = next->getPiece();
		if(strstr(typeid(*p).name(), "Pawn") != NULL) {
			pawn = (Pawn*)next->getPiece();
			return pawn->isPassing();
		}
	}
	return false;
}

bool Pawn::isPassing() {
	Logger::Move* lastMove = board->getLogger()->getLastMove();
	if(lastMove == NULL) {
		return false;
	}
	return passing && (lastMove->getEnd() == square->toString());
}

Piece* Pawn::promote() {
	// Promote the pawn to another piece
	board->setMessage(Board::MSG_PROMOTE);
	board->setMessageData(this);
	board->notify();
	return this;
}

void Pawn::promote(int newIdentity) {
	if(board->getMessage() != Board::MSG_PROMOTE) {
		return;
	}
	Piece* newPiece;
	switch(newIdentity) {
	case IDENTITY_ROOK:
		std::cout << "promote to rook" << std::endl;
		newPiece = new Rook(getColor());
		break;
	case IDENTITY_KNIGHT:
		std::cout << "promote to knight" << std::endl;
		newPiece = new Knight(getColor());
		break;
	case IDENTITY_BISHOP:
		std::cout << "promote to bishop" << std::endl;
		newPiece = new Bishop(getColor());
		break;
	case IDENTITY_QUEEN:
		std::cout << "promote to queen" << std::endl;
		newPiece = new Queen(getColor());
		break;
	default:
		return;
	}
	square->setPiece(newPiece);
	newPiece->setBoard(board);
	newPiece->setSquare(square);
	getOwner()->removePiece(this);
	getOwner()->addPiece(newPiece);
	board->setMessage(Board::MSG_PROMOTED);
	board->setMessageData((void*)&newIdentity);
	board->notify();
}

void Pawn::simulateMove(char file, int rank) {
	Piece* p; // The passing pawn
	if(canCaptureEnPassant(file)) {
		p = board->getSquare(file, square->getRank())->getPiece();
		p->getOwner()->removePiece(p);
		p->getSquare()->setPiece(NULL);
		p->setSquare(NULL);
	}
	// After capturing the passing pawn, move as usual
	Piece::simulateMove(file, rank);
}

bool Pawn::move(char file, int rank) {
	int startRank = getSquare()->getRank();
	bool mv = Piece::move(file, rank);
	// If the pawn was moved two squares
	if(mv && abs(rank - startRank) == 2) {
		// The pawn is passing (can be captured `en passant')
		passing = true;
	} else {
		passing = false;
	}
	if(rank == 8 || rank == 1) {
		promote();
	}
	return mv;
}

void Pawn::takeBoardSnapshot(char file, int rank) {
	if(canCaptureEnPassant(file)) {
		Square* end = board->getSquare(file, rank);
		// The auxiliaty square is the one with the passing pawn
		Square* aux = board->getSquare(file, square->getRank());
		board->saveBeforeMove(square, end, aux);
	} else {
		Piece::takeBoardSnapshot(file, rank);
	}
}

