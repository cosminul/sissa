#include "observable.h"

Observable::Observable() {
	observers.reserve(2);
}

Observable::~Observable() {
}

void Observable::attach(Observer* observer) {
	observers.push_back(observer);
}

void Observable::detach(Observer* observer) {
	std::vector<Observer*>::iterator i = observers.begin();
	while(i != observers.end()) {
		if((*i) == observer) {
			i = observers.erase(i);
		} else {
			i++;
		}
	}
}

void Observable::notify() {
	std::vector<Observer*>::iterator i;
	for(i = observers.begin(); i != observers.end(); i++) {
		(*i)->update(this);
	}
}

