#ifndef SISSA_H
#define SISSA_H

/* Main include header for the Sissa library */

#include "bishop.h"
#include "board.h"
#include "king.h"
#include "knight.h"
#include "logger.h"
#include "pawn.h"
#include "piece.h"
#include "player.h"
#include "queen.h"
#include "rook.h"
#include "sissa.h"
#include "square.h"

#endif

