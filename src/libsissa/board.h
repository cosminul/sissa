#ifndef BOARD_H
#define BOARD_H

#include "square.h"
#include "logger.h"
#include "observable.h"

class Memento;
class Player;
class Piece;

/**
 * A Board is a collection of Squares.
 *
 * @date 2006 11 16
 */
class Board : public Observable {
public:
	/**
	 * A state usually remembers two squares and their pieces. A third
	 * pair is remembered when capturing `en passant'.
	 */
	class State {
	private:
		Square* startSquare;
		Square* endSquare;
		Square* auxSquare;

		Piece* startSquarePiece;
		Piece* endSquarePiece;
		Piece* auxSquarePiece;

	public:
		State(Square* start, Square* end);

		Square* getStartSquare();

		void setStartSquare(Square* square);

		Square* getEndSquare();

		void setEndSquare(Square* square);

		Square* getAuxSquare();

		void setAuxSquare(Square* square);

		Piece* getStartSquarePiece();

		void setStartSquarePiece(Piece* piece);

		Piece* getEndSquarePiece();

		void setEndSquarePiece(Piece* piece);

		Piece* getAuxSquarePiece();

		void setAuxSquarePiece(Piece* piece);
	};

private:
	/** A bidimensional array of pointers to Squares */
	Square*** squares;

	/** A pointer to the white player */
	Player* white;

	/** A pointer to the black player */
	Player* black;

	/** A pointer to the current player */
	Player* current;

	State* state;

	Logger* logger;

	int message;

	void* messageData;

	void initPiece(Piece* piece, unsigned char file, int row);

public:
	static const int MSG_NONE = 0;
	static const int MSG_CHECK = 1;
	static const int MSG_CHECKMATE = 2;
	static const int MSG_PROMOTE = 3;
	static const int MSG_PROMOTED = 4;
	static const int MSG_JOIN = 5;
	static const int MSG_LEAVE = 6;
	static const int MSG_DRAW = 7;
	static const int MSG_RESIGN = 8;
	static const int MSG_YES_DRAW = 9;
	static const int MSG_NO_DRAW = 10;

	Board();

	~Board();

	void init();

	/**
	 * Picks up the piece located at specified coordinates on the board.
	 * @param file
	 * @param rank
	 * @return the piece picked up, or NULL if there was nothing there
	 */
	Piece* pickUp(char file, int rank);

	Square* getSquare(char file, int rank);

	Player* getWhite();

	void setWhite(Player* white);

	Player* getBlack();

	void setBlack(Player* black);

	Player* getCurrent();

	void setCurrent(Player* current);

	/** Changes the current player. */
	void endTurn();

	Logger* getLogger();

	void setLogger(Logger* logger);

	int getMessage();

	void setMessage(int message);

	void clearMessage();

	void* getMessageData();

	void setMessageData(void* data);

	State* getState();

	void saveBeforeMove(Square* start, Square* end);

	void saveBeforeMove(Square* start, Square* end, Square* aux);

	Memento* saveToMemento();

	void restoreFromMemento(Memento* memento);
};

#endif

