#include "memento.h"

Memento::Memento(Board::State* state) {
	this->state = state;
}

Memento::~Memento() {
}

Board::State* Memento::getSavedState() {
	return state;
}

