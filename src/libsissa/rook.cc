#include "rook.h"
#include "square.h"
#include "board.h"

Rook::Rook(int color) {
	name = "rook";
	letter = "R";
	this->color = color;
	moved = false;
	valid.reserve(14);
}

Rook::~Rook() {
	// bye bye, knight
}

/**
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> Rook::unsafeValidMoves() {
	valid.clear();
	addToValid(Piece::DIRECTION_N);
	addToValid(Piece::DIRECTION_E);
	addToValid(Piece::DIRECTION_S);
	addToValid(Piece::DIRECTION_W);
	return valid;
}

int Rook::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

