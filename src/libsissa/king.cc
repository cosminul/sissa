#include "king.h"
#include "square.h"
#include "board.h"

#include <iostream>

King::King(int color) {
	name = "king";
	letter = "K";
	this->color = color;
	moved = false;
	// Reserve 8 positions. When castling, the king is on the edge,
	// and the 2 extra positions are less than the 3 ones missing
	valid.reserve(8);
}

King::~King() {
	// bye bye, king
}

bool King::canCastleOnTheKingside() {
	// The player must never have moved the king
	if(wasMoved()) {
		return false;
	}
	// The king and the rook must be on the same rank
	Piece* rook = board->getSquare('h', square->getRank())->getPiece();
	// The player must never have moved the rook involved in castling
	if(rook == NULL || rook->wasMoved()) {
		return false;
	}
	// There must be no pieces between the king and the rook
	for(char f = square->getFile() + 1; f < 'h'; f++) {
		if(board->getSquare(f, square->getRank())->getPiece()) {
			return false;
		}
	}
	return true;
}

bool King::canCastleOnTheQueenside() {
	// The player must never have moved the king
	if(wasMoved()) {
		return false;
	}
	// The king and the rook must be on the same rank
	Piece* rook = board->getSquare('a', square->getRank())->getPiece();
	// The player must never have moved the rook involved in castling
	if(rook == NULL || rook->wasMoved()) {
		return false;
	}
	// There must be no pieces between the king and the rook
	for(char f = square->getFile() - 1; f > 'a'; f--) {
		if(board->getSquare(f, square->getRank())->getPiece()) {
			return false;
		}
	}
	return true;
}

/**
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> King::unsafeValidMoves() {
	valid.clear();
	char file = square->getFile();
	int rank = square->getRank();
	addToValid(file, rank + 1); // N
	addToValid(file + 1, rank + 1); // NE
	addToValid(file + 1, rank); // E
	addToValid(file + 1, rank - 1); // SE
	addToValid(file, rank - 1); // S
	addToValid(file - 1, rank - 1); // SW
	addToValid(file - 1, rank); // W
	addToValid(file - 1, rank + 1); // NW
	if(canCastleOnTheKingside()) {
		addToValid('g', rank);
	}
	if(canCastleOnTheQueenside()) {
		addToValid('c', rank);
	}
	return valid;
}

void King::ignoreUnsafeMoves() {
	Piece::ignoreUnsafeMoves();
	// Check castling restrictions
	if(canCastleOnTheKingside()) {
		// When castling, the king may not currently be in check
		if(getOwner()->kingIsInCheck()) {
			removeFromValid('g', square->getRank());
		}
		// The king cannot pass through squares that are under attack
		// by enemy pieces.
		if(!isSafeToMoveAt('f', square->getRank())) {
			removeFromValid('g', square->getRank());
		}
	}
	if(canCastleOnTheQueenside()) {
		if(getOwner()->kingIsInCheck()) {
			removeFromValid('c', square->getRank());
		}
		if(!isSafeToMoveAt('d', square->getRank())) {
			removeFromValid('c', square->getRank());
		}
	}
}

bool King::move(char file, int rank) {
	Square* oldSquare = getSquare();
	if(canMoveTo(file, rank) == false) {
		return false;
	}
	int span = file - square->getFile();
	simulateMove(file, rank);
	Logger::Move* loggedMove;
	Piece* rook;
	int special = -1;
	if(span == 2) {
		// Kingside castling
		rook = board->getSquare('h', square->getRank())->getPiece();
		// Calling rook's `simulateMove' method instead of `move'
		// to avoid the `canMoveTo' check (which would return false).
		rook->simulateMove('f', square->getRank());
		special = Logger::Move::KINGSIDE_CASTLING;
	} else if(span == -2) {
		// Queenside castling
		rook = board->getSquare('a', square->getRank())->getPiece();
		rook->simulateMove('d', square->getRank());
		special = Logger::Move::QUEENSIDE_CASTLING;
	}
	if(special == -1) {
		loggedMove = new Logger::Move(oldSquare, getSquare());
	} else {
		loggedMove = new Logger::Move(oldSquare, getSquare(), special);
	}
	board->getLogger()->log(loggedMove);
	changeBoardMessage(); // Say 'checkmate!', or 'check!' if needed
	moved = true;
	board->endTurn();

	return true;
}

int King::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

