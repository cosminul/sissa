#include "knight.h"
#include "square.h"
#include "board.h"

Knight::Knight(int color) {
	name = "knight";
	letter = "N"; // "K" is used by the King. Could also use "Kt"
	this->color = color;
	moved = false;
	valid.reserve(8);
}

Knight::~Knight() {
	// bye bye, knight
}

/**
 * @return a vector of pointers to Squares where we can move
 */
std::vector<Square*> Knight::unsafeValidMoves() {
	valid.clear();
	char file = square->getFile();
	int rank = square->getRank();
	addToValid(file + 1, rank + 2); // NNE
	addToValid(file + 2, rank + 1); // ENE
	addToValid(file + 2, rank - 1); // ESE
	addToValid(file + 1, rank - 2); // SSE
	addToValid(file - 1, rank - 2); // SSW
	addToValid(file - 2, rank - 1); // WSW
	addToValid(file - 2, rank + 1); // WNW
	addToValid(file - 1, rank + 2); // NNW
	return valid;
}

int Knight::getAppearance() {
	if(getColor() == Piece::WHITE) {
		return WHITE;
	} else {
		return BLACK;
	}
}

