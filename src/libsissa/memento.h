#ifndef MEMENTO_H
#define MEMENTO_H

#include "board.h"

/**
 * Memento - used in the Memento design pattern.
 * Memento objects have two interfaces: an extended one, for the Originators,
 * and a reduced one, for the other objects.
 * In this implementation, the Originator is a Board::State object.
 * @author Cosmin
 * @date 2007 30 29
 */
class Memento {
private:
	Board::State* state;

private:
	// Private members accesible only to the Board object
	friend class Board;

	Memento(Board::State* state);

	Board::State* getSavedState();

public:
	virtual ~Memento();
};

#endif
