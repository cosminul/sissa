#ifndef SQUARE_H
#define SQUARE_H

#include <string>

// Using forward declaration instead of include directive
class Piece;

/**
 * Square
 *
 * @date 2006 11 03
 */
class Square {
private:
	/** The column */
	char file;

	/** The row */
	int rank;

	/** The color of the square */
	int color;

	/** The piece on the square, or null */
	Piece* piece;
public:
	/** The white (light) color */
	static const int WHITE = 0;
	/** The black (dark) color */
	static const int BLACK = 1;

	Square();

	Square(char file, int rank);

	~Square();

	char getFile();

	int getRank();

	int getColor();

	void setColor(int color);

	Piece* getPiece();

	void setPiece(Piece* piece);

	/** Is there a piece on this square? */
	bool isFree();

	/** Two squares are friends if they both have same-colored pieces. */
	bool isFriendOf(Square* s);

	/**
	 * @return true if and only if there is a piece on this square
	 * and it belongs to the opponent; false otherwise
	 */
	bool isOpponentOf(int color);

	std::string toString();
};

#endif

