#ifndef LONG_ALG_LOGGER
#define LONG_ALG_LOGGER

#include "logger.h"

class LongAlgebraicLogger : public Logger {
public:
	LongAlgebraicLogger();

	~LongAlgebraicLogger();

	std::string toString();
};

#endif

