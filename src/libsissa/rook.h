#ifndef ROOK_H
#define ROOK_H

#include "piece.h"

/**
 * Rook is a Piece.
 *
 * @date 2006 12 18
 */
class Rook : public Piece {
public:
	static const int WHITE = 4;
	static const int BLACK = 5;

	Rook(int color);

	~Rook();

	std::vector<Square*> unsafeValidMoves();

	int getAppearance();
};

#endif

