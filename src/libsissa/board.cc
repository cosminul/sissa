#include "board.h"
#include "pawn.h"
#include "knight.h"
#include "bishop.h"
#include "rook.h"
#include "queen.h"
#include "king.h"
#include "memento.h"
#include "longAlgLogger.h"

Board::Board() {
	// There are no players yet
	white = black = current = NULL;
	// Initialize the squares
	squares = new Square**[8];
	for(unsigned char file = 0; file < 8; file++) {
		squares[file] = new Square*[8];
		for(int rank = 0; rank < 8; rank++) {
			squares[file][rank] = new Square(file + 'a', rank + 1);
		}
	}
	state = new Board::State(NULL, NULL);
	logger = new LongAlgebraicLogger();
	message = 0;
	messageData = NULL;
}

Board::~Board() {
}

void Board::init() {
	logger->clear();
	for(unsigned char file = 0; file < 8; file++) {
		for(int rank = 0; rank < 8; rank++) {
			squares[file][rank]->setPiece(NULL);
		}
	}
	// Place white pieces
	initPiece(new Rook(Piece::WHITE), 0, 0);
	initPiece(new Knight(Piece::WHITE), 1, 0);
	initPiece(new Bishop(Piece::WHITE), 2, 0);
	initPiece(new Queen(Piece::WHITE), 3, 0);
	initPiece(new King(Piece::WHITE), 4, 0);
	initPiece(new Bishop(Piece::WHITE), 5, 0);
	initPiece(new Knight(Piece::WHITE), 6, 0);
	initPiece(new Rook(Piece::WHITE), 7, 0);
	for(int i = 0; i < 8; i++) {
		initPiece(new Pawn(Piece::WHITE), i, 1);
	}
	// Place black pieces
	for(int i = 0; i < 8; i++) {
		initPiece(new Pawn(Piece::BLACK), i, 6);
	}
	initPiece(new Rook(Piece::BLACK), 0, 7);
	initPiece(new Knight(Piece::BLACK), 1, 7);
	initPiece(new Bishop(Piece::BLACK), 2, 7);
	initPiece(new Queen(Piece::BLACK), 3, 7);
	initPiece(new King(Piece::BLACK), 4, 7);
	initPiece(new Bishop(Piece::BLACK), 5, 7);
	initPiece(new Knight(Piece::BLACK), 6, 7);
	initPiece(new Rook(Piece::BLACK), 7, 7);
}

Piece* Board::pickUp(char file, int rank) {
	if(getSquare(file, rank) == NULL) {
		return NULL;
	}
	return getSquare(file, rank)->getPiece();
}

Square* Board::getSquare(char file, int rank) {
	if(file < 'a' || file > 'h') {
		return NULL;
	}
	if(rank < 1 || rank > 8) {
		return NULL;
	}
	return squares[file - 'a'][rank - 1];
}

Player* Board::getWhite() {
	return white;
}

void Board::setWhite(Player* white) {
	this->white = white;
	// The board's state has changed, notify the observers.
	notify();
}

Player* Board::getBlack() {
	return black;
}

void Board::setBlack(Player* black) {
	this->black = black;
	// The board's state has changed, notify the observers.
	notify();
}

Player* Board::getCurrent() {
	return current;
}

void Board::setCurrent(Player* current) {
	this->current = current;
	// The board's state has changed, notify the observers.
	notify();
}

void Board::initPiece(Piece* piece, unsigned char file, int row) {
	squares[file][row]->setPiece(piece);
	piece->setBoard(this);
	piece->setSquare(squares[file][row]);
}

void Board::endTurn() {
	setCurrent((current == white) ? black : white);
}

Logger* Board::getLogger() {
	return logger;
}

int Board::getMessage() {
	return message;
}

void Board::setMessage(int message) {
	this->message = message;
}

void Board::clearMessage() {
	message = MSG_NONE;
	messageData = NULL;
}

void* Board::getMessageData() {
	return messageData;
}

void Board::setMessageData(void* data) {
	messageData = data;
}

void Board::setLogger(Logger* logger) {
	this->logger = logger;
}

Board::State* Board::getState() {
	return state;
}

void Board::saveBeforeMove(Square* start, Square* end) {
	saveBeforeMove(start, end, NULL);
}

void Board::saveBeforeMove(Square* start, Square* end, Square* aux) {
	state->setStartSquare(start);
	state->setEndSquare(end);
	state->setStartSquarePiece(start->getPiece());
	state->setEndSquarePiece(end->getPiece());
	state->setAuxSquare(aux);
	if(aux != NULL) {
		state->setAuxSquarePiece(aux->getPiece());
	}
}

Memento* Board::saveToMemento() {
	return new Memento(state);
}

void Board::restoreFromMemento(Memento* memento) {
	state = memento->getSavedState();

	Square* startSquare = state->getStartSquare();
	Square* endSquare = state->getEndSquare();
	Square* auxSquare = state->getAuxSquare();

	Piece* startPiece = state->getStartSquarePiece();
	Piece* endPiece = state->getEndSquarePiece();
	Piece* auxPiece = state->getAuxSquarePiece();

	startSquare->setPiece(startPiece);
	endSquare->setPiece(endPiece);
	if(startPiece != NULL) {
		startPiece->setSquare(startSquare);
	}
	if(endPiece != NULL) {
		endPiece->setSquare(endSquare);
		// If an end piece existed, it was removed from
		// the owner's collection, so it needs to be added back
		endPiece->getOwner()->addPiece(endPiece);
	}
	if(auxSquare != NULL) {
		auxSquare->setPiece(auxPiece);
		if(auxPiece != NULL) {
			auxPiece->setSquare(auxSquare);
			auxPiece->getOwner()->addPiece(auxPiece);
		}
	}
}

Board::State::State(Square* start, Square* end) {
	startSquare = start;
	endSquare = end;
	auxSquare = NULL;
	startSquarePiece = endSquarePiece = auxSquarePiece = NULL;
	if(start != NULL) {
		startSquarePiece = start->getPiece();
	}
	if(end != NULL) {
		endSquarePiece = end->getPiece();
	}
}

Square* Board::State::getStartSquare() {
	return startSquare;
}

void Board::State::setStartSquare(Square* square) {
	startSquare = square;
}

Square* Board::State::getEndSquare() {
	return endSquare;
}

void Board::State::setEndSquare(Square* square) {
	endSquare = square;
}

Square* Board::State::getAuxSquare() {
	return auxSquare;
}

void Board::State::setAuxSquare(Square* square) {
	auxSquare = square;
}

Piece* Board::State::getStartSquarePiece() {
	return startSquarePiece;
}

void Board::State::setStartSquarePiece(Piece* piece) {
	startSquarePiece = piece;
}

Piece* Board::State::getEndSquarePiece() {
	return endSquarePiece;
}

void Board::State::setEndSquarePiece(Piece* piece) {
	endSquarePiece = piece;
}

Piece* Board::State::getAuxSquarePiece() {
	return auxSquarePiece;
}

void Board::State::setAuxSquarePiece(Piece* piece) {
	auxSquarePiece = piece;
}

