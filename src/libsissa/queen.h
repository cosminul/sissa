#ifndef QUEEN_H
#define QUEEN_H

#include "piece.h"

/**
 * Queen is a Piece.
 *
 * @date 2006 12 18
 */
class Queen : public Piece {
public:
	static const int WHITE = 2;
	static const int BLACK = 3;

	Queen(int color);

	~Queen();

	std::vector<Square*> unsafeValidMoves();

	int getAppearance();
};

#endif

