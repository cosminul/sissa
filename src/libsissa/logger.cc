#include "logger.h"
#include "piece.h"

Logger::Logger() {
	moves.reserve(20);
}

Logger::~Logger() {
}

Logger::Move* Logger::getLastMove() {
	if(moves.size() == 0) {
		return NULL;
	}
	return moves.back();
}

void Logger::log(Move* move) {
	moves.push_back(move);
}

void Logger::clear() {
	moves.clear();
}

Logger::Move::Move(Square* start, Square* end) {
	this->start = start->toString();
	this->end = end->toString();
	piece = end->getPiece()->getLetter();
	special = 0;
}

Logger::Move::Move(Square* start, Square* end, int specialMove) {
	this->start = start->toString();
	this->end = end->toString();
	piece = end->getPiece()->getLetter();
	special = specialMove;
}

std::string Logger::Move::getStart() {
	return start;
}

std::string Logger::Move::getEnd() {
	return end;
}

std::string Logger::Move::getPiece() {
	return piece;
}

int Logger::Move::getSpecial() {
	return special;
}

