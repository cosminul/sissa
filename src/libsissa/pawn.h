#ifndef PAWN_H
#define PAWN_H

#include "piece.h"

/**
 * Pawn is a Piece.
 *
 * @date 2006 11 14
 */
class Pawn : public Piece {
private:
	/** The advancing direction: 1 or -1, depending on the color */
	int advance;
	/** The home rank: 1+1 or 8-1, depending on the color */
	int homeRank;
	/** Whether this Pawn is passing and can be captured `en passant' */
	bool passing;

	/**
	 * Promotes the pawn to a rook, knight, bishop or queen.
	 * @return a pointer to the new piece
 	 */
	Piece* promote();

	bool canCaptureEnPassant(char file);

	void takeBoardSnapshot(char file, int rank);

public:
	static const int WHITE = 8;
	static const int BLACK = 9;

	/** Used for promotion to rook */
	static const int IDENTITY_ROOK = 20;
	/** Used for promotion to knight */
	static const int IDENTITY_KNIGHT = 21;
	/** Used for promotion to bishop */
	static const int IDENTITY_BISHOP = 22;
	/** Used for promotion to queen */
	static const int IDENTITY_QUEEN = 24;

	Pawn(int color);

	~Pawn();

	std::vector<Square*> unsafeValidMoves();

	int getAppearance();

	bool isPassing();

	/**
	 * Promotes the pawn to a rook, knight, bishop or queen.
	 * @param newIdentity one of the identity constants (IDENTITY_*)
 	 */
	void promote(int newIdentity);

	void simulateMove(char file, int rank);

	bool move(char file, int rank);
};

#endif

