# sissa
libSDL implementation of a network chess game

![Screenshot](screenshot/sissa_screenshot.png)

## Build

    $ cd src
    $ make
    $ ../bin/gui
